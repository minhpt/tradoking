<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $db = dbFireBase();
//        $db->getReference('config/website')
//            ->set([
//                'name' => 'My Application',
//                'emails' => [
//                    'support' => 'support@domain.tld',
//                    'sales' => 'sales@domain.tld',
//                ],
//                'website' => 'https://app.domain.tld',
//            ]);

//        $a = $db->getReference()->getValue();
        return view('admin::index');
    }
    public function logout()
    {
        session()->forget('user');
        return redirect()->route('admin.login');
    }
}
