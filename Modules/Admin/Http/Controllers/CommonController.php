<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Routing\Controller;

class CommonController extends Controller
{
    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Update file
     */
    public function uploadFile()
    {
        return response()->json(['status' => true]);
    }

    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Get Views Pages
     */
    public function pagination($current_page, $total_page)
    {
        return view('admin::pagination.index')
            ->with('current_page',(int)$current_page)
            ->with('total_page',(int)$total_page);
    }

}