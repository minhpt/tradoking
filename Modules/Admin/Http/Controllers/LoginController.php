<?php


namespace Modules\Admin\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class LoginController extends Controller
{
    private $db;
    private $object;

    public function __construct()
    {
        $this->db = dbFireBase()->getDatabase();
        $this->object = 'staffs';
    }

    public function index(Request $request)
    {
        if ($request->isMethod('GET')) {
            if(session()->has('user'))
                return redirect()->route('admin.index');
            return view('admin::login.index');
        }
        $email = $request->email;
        $password = $request->password;
        $users = $this->db->getReference($this->object)->orderByChild('email')->equalTo($email)->limitToFirst(1)->getSnapshot();
        messageSession($request, 'danger', 'Tài khoản không tồn tại!');
        if (!is_null($users->getValue())) {
            $result = $users->getValue();
            $index = array_search(md5($password), array_column($result, 'password'));
            messageSession($request, 'danger', 'Mật khẩu không đúng!');
            if($index !== false)
            {
                $key = array_keys($result)[0];
                session(['user' => $result[$key]]);
                return redirect()->route('admin.index');
            }
        }
        return redirect()->route('admin.login');

    }
}