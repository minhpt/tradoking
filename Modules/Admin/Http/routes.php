<?php

Route::group(['middleware' => ['web','auth.ptm'], 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::get('/', 'AdminController@index')->name('admin.index');
    Route::post('/upload', 'CommonController@uploadFile');
    Route::get('/logout', 'AdminController@logout')->name('admin.logout');

    //pagination
    Route::get('/pagination/{current_page}/{total_page}', 'CommonController@pagination');
});
Route::group(['middleware' => ['web'], 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::get('/login', 'LoginController@index')->name('admin.login');
    Route::post('/login', 'LoginController@index')->name('admin.login');
});
