@extends('admin::layouts.master')
@section('title')
    Home
@endsection
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Phòng
            </h1>
            <a href="{{route('admin-room.room.create')}}" class="btn btn-primary btn-box-above">Tạo phòng</a>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Phòng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Hello World</h1>
                    <p>
                        This view is loaded from module: {!! config('admin.name') !!}
                    </p>
                    {{--<pre id="object">--}}

                    {{--</pre>--}}
                </div>
            </div>
        </section>
    </div>
@stop
@push('scripts')
    <script src="{{asset('/modules/common/js/firebase.js')}}"></script>
    <script>
        // Initialize Firebase
        $(function () {
            var config = {
                apiKey: "AIzaSyBi7pb9Xm1dtRbd2ztQ1okHxZN0RFGBzRo",
                authDomain: "tradoking-92b53.firebaseapp.com",
                databaseURL: "https://tradoking-92b53.firebaseio.com",
                projectId: "tradoking-92b53",
                storageBucket: "tradoking-92b53.appspot.com",
                messagingSenderId: "803878142073"
            };
            firebase.initializeApp(config);

            const preObject = document.getElementById('object');

            const dbRefObject = firebase.database().ref().child('config');

            dbRefObject.on('value', snap => (preObject.innerText = JSON.stringify(snap.val(), null, 3)));
        });

    </script>
@endpush