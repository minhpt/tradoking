<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{asset('assets/admin/dist/img/user.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
            <p>{{user()['name']['first'] . ' ' . user()['name']['last']}}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
            <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
    </form>
    <!-- /.search form -->
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li>
            <a href="#">
                <i class="fa fa-home"></i> <span>Trang chủ</span>
                <span class="pull-right-container">
            </span>
            </a>
        </li>
        <li class="users">
            <a href="{{route('admin-users.user.index')}}">
                <i class="fa fa-users"></i> <span>Người Dùng</span>
                <span class="pull-right-container">
            </span>
            </a>
        </li>
        <li class="rooms">
            <a href="{{route('admin-room.room.index')}}">
                <i class="fa fa-bed"></i> <span>Phòng</span>
                <span class="pull-right-container">
            </span>
            </a>
        </li>
        <li class="transactions">
            <a href="{{route('admin-transactions.transaction.index')}}">
                <i class="fa fa-refresh"></i> <span>Giao dịch</span>
                <span class="pull-right-container">
            </span>
            </a>
        </li>
        <li class="treeview categories">
            <a href="#">
                <i class="fa fa-list"></i> <span>Danh mục</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="hotel"><a href="{{route('admin-categories.hotel.index')}}"><i class="fa fa-circle-o"></i> Khách sạn</a></li>
                <li class="view"><a href="{{route('admin-categories.view.index')}}"><i class="fa fa-circle-o"></i> Hướng nhìn</a></li>
                <li class="extension"><a href="{{route('admin-categories.extension.index')}}"><i class="fa fa-circle-o"></i> Tiện ích</a></li>
                <li class="type"><a href="{{route('admin-categories.type.index')}}"><i class="fa fa-circle-o"></i> Loại Phòng</a></li>
            </ul>
        </li>
        <li class="treeview system">
            <a href="#">
                <i class="fa fa-gears"></i> <span>Hệ thống</span>
                <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
            </a>
            <ul class="treeview-menu">
                <li class="role"><a href="{{route('admin-system.role.index')}}"><i class="fa fa-circle-o"></i> Phân Quyền</a></li>
                <li class="user-admin"><a href="{{route('admin-system.user.index')}}"><i class="fa fa-circle-o"></i> Quản trị</a></li>
            </ul>
        </li>
    </ul>
</section>
<!-- /.sidebar -->
</aside>