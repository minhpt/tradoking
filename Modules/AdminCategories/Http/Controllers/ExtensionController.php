<?php
/**
 * Created by PhpStorm.
 * User: Thanh Minh
 * Date: 3/17/2018
 * Time: 11:24
 */

namespace Modules\AdminCategories\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ExtensionController extends Controller
{
    private $db;
    private $object;
    public function __construct()
    {
        $this->db = dbFireBase()->getDatabase();
        $this->object = 'dataInfo/hotelBenefits';
    }

    public function index()
    {
        $extensions = $this->db->getReference($this->object)->getSnapshot();
        return view('admincategories::extension.index', ['extensions' => $extensions]);
    }

    public function create(Request $request)
    {
        $extensions = $this->db->getReference($this->object)->getSnapshot()->getValue();
        $id = max(array_keys($extensions)) + 1;
        if ($request->isMethod('GET')) {
            return view('admincategories::extension.create',['id' => $id]);
        }
        $array = $request->all();
        array_shift($array);
        $extensions[$id] = $array[0];
        $this->db->getReference($this->object)->set($extensions);
        messageSession($request, 'success', 'Thêm thành công');
        return redirect()->route('admin-categories.extension.index');
    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('GET')) {
            $extension = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
            if (!$extension->exists()) return abort(404);
            return view('admincategories::extension.edit', ['extension' => $extension,'id' => $id]);
        }
        $array = $request->all();
        array_shift($array);
        foreach ($array as $key => $value) {
            $this->db->getReference($this->object .'/' . $id)->set($value);
        }
        messageSession($request, 'success', 'Cập nhật thành công');
        return redirect()->route('admin-categories.extension.index');
    }

    public function detail($id)
    {
        $extension = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
        return view('admincategories::extension.detail',['extension' => $extension]);
    }

    public function delete($id)
    {
        $extension = $this->db->getReference($this->object . '/' . $id)->remove();
        return response()->json($extension);
    }
}