<?php
/**
 * Created by PhpStorm.
 * User: Thanh Minh
 * Date: 2/6/2018
 * Time: 19:01
 */

namespace Modules\AdminCategories\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class HotelController extends Controller
{
    private $db;
    private $object;
    private $pageKey;
    private $records;
    public function __construct()
    {
        $this->db = dbFireBase()->getDatabase();
        $this->object = 'hotels';
        $this->records = 10;
        $this->pageKey = pageKey($this->db->getReference($this->object)->getValue(), $this->records);
    }

    public function index()
    {
        $hotels = $this->db->getReference($this->object)->orderByKey()->limitToFirst($this->records)->getSnapshot()->getValue();
        $pages = self::getPages();
        return view('admincategories::hotel.index',compact('hotels','pages'));
    }

    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Get record by page number
     */
    public function pagination($currentPage, $records, $search = null)
    {
        $hotels = $this->db->getReference($this->object)->orderByKey()->startAt($this->pageKey[$currentPage-1])->limitToFirst($this->records)->getValue();
        $pages = self::getPages();
        return view('admincategories::hotel.pagination', compact('pages', 'hotels'));
    }

    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Get total page
     */
    public function getPages()
    {
        return ceil($this->db->getReference($this->object)->getSnapshot()->numChildren()/$this->records);
    }

    public function create(Request $request)
    {
        if ($request->isMethod('GET')) {
            return view('admincategories::hotel.create');
        }
        $array = $request->all();
        array_shift($array);
        $array['createTime'] = strtotime(date('Y-m-d H:i:s'));
        $key = $this->db->getReference($this->object)->push($array)->getKey();
        $this->db->getReference($this->object. '/'.$key)->update(['hotelId' => $key]);
        messageSession($request, 'success', 'Thêm thành công');
        return redirect()->route('admin-categories.hotel.index');
    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('GET')) {
            $hotel = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
            if (!$hotel->exists()) return abort(404);
            return view('admincategories::hotel.edit', ['hotel' => $hotel]);
        }
        $array = $request->all();
        array_shift($array);
        $array['lastUpdate'] = strtotime(date('Y-m-d H:i:s'));
        foreach ($array as $key => $value) {
            $this->db->getReference($this->object .'/' . $id . '/' . $key)->set($value);
        }
        messageSession($request, 'success', 'Cập nhật thành công');
        return redirect()->route('admin-categories.hotel.index');
    }

    public function detail($id)
    {
        $hotel = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
        return view('admincategories::hotel.detail',['hotel' => $hotel]);
    }

    public function delete($id)
    {
        $hotel = $this->db->getReference($this->object . '/' . $id)->remove();
        return response()->json($hotel);
    }
}