<?php
/**
 * Created by PhpStorm.
 * User: Thanh Minh
 * Date: 3/17/2018
 * Time: 11:24
 */

namespace Modules\AdminCategories\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TypeRoomController extends Controller
{
    private $db;
    private $object;
    public function __construct()
    {
        $this->db = dbFireBase()->getDatabase();
        $this->object = 'dataInfo/typeOfRooms';
    }

    public function index()
    {
        $typeOfRoom = $this->db->getReference($this->object)->getSnapshot();
        return view('admincategories::type.index', ['types' => $typeOfRoom]);
    }

    public function create(Request $request)
    {
        $typeOfRoom = $this->db->getReference($this->object)->getSnapshot()->getValue();
        $id = max(array_keys($typeOfRoom)) + 1;
        if ($request->isMethod('GET')) {
            return view('admincategories::type.create',['id' => $id]);
        }
        $array = $request->all();
        array_shift($array);
        $typeOfRoom[$id] = $array[0];
        $this->db->getReference($this->object)->set($typeOfRoom);
        messageSession($request, 'success', 'Thêm thành công');
        return redirect()->route('admin-categories.type.index');
    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('GET')) {
            $type = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
            if (!$type->exists()) return abort(404);
            return view('admincategories::type.edit', ['type' => $type,'id' => $id]);
        }
        $array = $request->all();
        array_shift($array);
        foreach ($array as $key => $value) {
            $this->db->getReference($this->object .'/' . $id)->set($value);
        }
        messageSession($request, 'success', 'Cập nhật thành công');
        return redirect()->route('admin-categories.type.index');
    }

    public function detail($id)
    {
        $type = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
        return view('admincategories::type.detail',['type' => $type]);
    }

    public function delete($id)
    {
        $type = $this->db->getReference($this->object . '/' . $id)->remove();
        return response()->json($type);
    }
}