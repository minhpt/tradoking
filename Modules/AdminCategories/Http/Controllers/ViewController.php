<?php
/**
 * Created by PhpStorm.
 * User: Thanh Minh
 * Date: 3/17/2018
 * Time: 11:24
 */

namespace Modules\AdminCategories\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    private $db;
    private $object;
    public function __construct()
    {
        $this->db = dbFireBase()->getDatabase();
        $this->object = 'dataInfo/views';
    }

    public function index()
    {
        $views = $this->db->getReference($this->object)->getSnapshot();
        return view('admincategories::view.index', ['views' => $views]);
    }

    public function create(Request $request)
    {
        $views = $this->db->getReference($this->object)->getSnapshot()->getValue();
        $id = max(array_keys($views)) + 1;
        if ($request->isMethod('GET')) {
            return view('admincategories::view.create',['id' => $id]);
        }
        $array = $request->all();
        array_shift($array);
        $views[$id] = $array[0];
        $this->db->getReference($this->object)->set($views);
        messageSession($request, 'success', 'Thêm thành công');
        return redirect()->route('admin-categories.view.index');
    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('GET')) {
            $view = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
            if (!$view->exists()) return abort(404);
            return view('admincategories::view.edit', ['view' => $view,'id' => $id]);
        }
        $array = $request->all();
        array_shift($array);
        foreach ($array as $key => $value) {
            $this->db->getReference($this->object .'/' . $id)->set($value);
        }
        messageSession($request, 'success', 'Cập nhật thành công');
        return redirect()->route('admin-categories.view.index');
    }

    public function detail($id)
    {
        $view = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
        return view('admincategories::view.detail',['view' => $view]);
    }

    public function delete($id)
    {
        $view = $this->db->getReference($this->object . '/' . $id)->remove();
        return response()->json($view);
    }
}