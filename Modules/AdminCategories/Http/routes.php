<?php

Route::group(['middleware' => ['web','auth.ptm'], 'prefix' => 'admin', 'namespace' => 'Modules\AdminCategories\Http\Controllers'], function()
{
    Route::group(['prefix' => 'hotels'], function(){
        Route::get('/', 'HotelController@index')->name('admin-categories.hotel.index');
        Route::get('/create', 'HotelController@create')->name('admin-categories.hotel.create');
        Route::post('/create', 'HotelController@create')->name('admin-categories.hotel.create');
        Route::get('/edit/{id}', 'HotelController@edit')->name('admin-categories.hotel.edit');
        Route::post('/edit/{id}', 'HotelController@edit')->name('admin-categories.hotel.edit');
        Route::get('/detail/{id}', 'HotelController@detail')->name('admin-categories.hotel.detail');
        Route::get('/delete/{id}', 'HotelController@delete')->name('admin-categories.hotel.delete');

        //Pagination
        Route::get('/pagination/{currentPage}/{records}/{search?}', 'HotelController@pagination')->name('admin-room.room.pagination');
    });
    Route::group(['prefix' => 'views'], function(){
        Route::get('/', 'ViewController@index')->name('admin-categories.view.index');
        Route::get('/create', 'ViewController@create')->name('admin-categories.view.create');
        Route::post('/create', 'ViewController@create')->name('admin-categories.view.create');
        Route::get('/edit/{id}', 'ViewController@edit')->name('admin-categories.view.edit');
        Route::post('/edit/{id}', 'ViewController@edit')->name('admin-categories.view.edit');
        Route::get('/detail/{id}', 'ViewController@detail')->name('admin-categories.view.detail');
        Route::get('/delete/{id}', 'ViewController@delete')->name('admin-categories.view.delete');
    });
    Route::group(['prefix' => 'extensions'], function(){
        Route::get('/', 'ExtensionController@index')->name('admin-categories.extension.index');
        Route::get('/create', 'ExtensionController@create')->name('admin-categories.extension.create');
        Route::post('/create', 'ExtensionController@create')->name('admin-categories.extension.create');
        Route::get('/edit/{id}', 'ExtensionController@edit')->name('admin-categories.extension.edit');
        Route::post('/edit/{id}', 'ExtensionController@edit')->name('admin-categories.extension.edit');
        Route::get('/detail/{id}', 'ExtensionController@detail')->name('admin-categories.extension.detail');
        Route::get('/delete/{id}', 'ExtensionController@delete')->name('admin-categories.extension.delete');
    });
    Route::group(['prefix' => 'types'], function(){
        Route::get('/', 'TypeRoomController@index')->name('admin-categories.type.index');
        Route::get('/create', 'TypeRoomController@create')->name('admin-categories.type.create');
        Route::post('/create', 'TypeRoomController@create')->name('admin-categories.type.create');
        Route::get('/edit/{id}', 'TypeRoomController@edit')->name('admin-categories.type.edit');
        Route::post('/edit/{id}', 'TypeRoomController@edit')->name('admin-categories.type.edit');
        Route::get('/detail/{id}', 'TypeRoomController@detail')->name('admin-categories.type.detail');
        Route::get('/delete/{id}', 'TypeRoomController@delete')->name('admin-categories.type.delete');
    });
});
