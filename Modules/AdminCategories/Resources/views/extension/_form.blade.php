@php
    if(isset($extension))
        $extension = $extension->getValue();
@endphp
<div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-3 control-label">Tiện ích @include('require')</label>

                <div class="col-md-4">
                    <input type="text" class="form-control extension" id="view" name="{{isset($id) ? $id : null}}" value="{{isset($extension) ? $extension : null}}">
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/adminCategories/extension-validate.js')}}"></script>
@endpush