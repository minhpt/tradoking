@push('style')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.3/img/loading.gif">
@endpush
@php
    if(isset($hotel))
        $hotel = $hotel->getValue();
@endphp
<div class="box-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-3 control-label">Tên @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="name" name="name"
                               value="@isset($hotel) {{$hotel['name']}} @endisset">
                    </div>
                </div>
            </div>
            {{--<div class="form-group">--}}
                {{--<label for="inputPassword3" class="col-md-3 control-label">Địa chỉ @include('require')</label>--}}

                {{--<div class="col-md-4">--}}
                    {{--<input type="text" class="form-control" id="address" name="address" value="@isset($hotel)  {{$hotel['address']}} @endisset">--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Giá cao nhất @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="priceMax" name="priceMax"
                               value="@isset($hotel)  {{$hotel['priceMax']}} @endisset">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Giá thấp nhất @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="priceMax" name="priceMin"
                               value="@isset($hotel)  {{$hotel['priceMin']}} @endisset">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Sao</label>

                <div class="col-md-4">
                    <input id="starRate" type="text" class="rating" data-size="xs" name="starRate" value="@isset($hotel)  {{$hotel['starRate']}} @endisset">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Địa chỉ @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" name="address" id="maps_address" value="@isset($hotel)  {{$hotel['address']}} @endisset" placeholder="Nhập tên địa điểm cần tìm">
                    </div>
                    <div id="maps_maparea">
                        <div id="maps_mapcanvas" class="form-group"></div>
                        <div class="row hidden">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">L</span>
                                        <input type="text" class="form-control" id="maps_mapcenterlat" value="@isset($hotel) {{$hotel['latitude']}} @endisset" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">N</span>
                                        <input type="text" class="form-control" id="maps_mapcenterlng" value="@isset($hotel) {{$hotel['longitude']}} @endisset" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">L</span>
                                        <input type="text" class="form-control" name="latitude" id="maps_maplat" value="@isset($hotel) {{$hotel['latitude']}} @endisset"readonly="readonly">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">N</span>
                                        <input type="text" class="form-control" name="longitude" id="maps_maplng" value="@isset($hotel) {{$hotel['longitude']}} @endisset" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row hidden">
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Z</span>
                                        <input type="text" class="form-control" id="maps_mapzoom" value="15" readonly="readonly">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <!-- DataTables -->
    <!-- default styles -->
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.3/css/star-rating.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.3/themes/krajee-fa/theme.css">

    <!-- optionally if you need translation for your language then include locale file as mentioned below -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.3/js/star-rating.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.3/themes/krajee-fa/theme.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.3/js/locales/es.js"></script>
    <script>
        $(function () {
            activeMenu('categories', 'hotel', true);
            $("#starRate").rating();
        })
    </script>
    <script src="{{asset('modules/adminCategories/hotel.js')}}"></script>
    <script src="{{asset('modules/adminCategories/hotel-validater.js')}}"></script>
@endpush