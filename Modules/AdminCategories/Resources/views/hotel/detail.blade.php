@extends('admin::layouts.master')
@section('title')
    Danh sách khách sạn
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('modules/Adminhotels/css/hotel.css')}}">
@endpush
@php
    if(isset($hotel))
        $hotel = $hotel->getValue();
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               khách sạn
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">khách sạn</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin khách sạn</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-offset-3">
                                    <table class="table table-bordered table-detail">
                                        <tr>
                                            <th>Tên</th>
                                            <td>Mường Thanh</td>
                                        </tr>
                                        <tr>
                                            <th>Địa chỉ</th>
                                            <td>Blabla</td>
                                        </tr>
                                        <tr>
                                            <th>Giá cao nhất</th>
                                            <td>{{formatCurrency($hotel['priceMax'])}}</td>
                                        </tr>
                                        <tr>
                                            <th>Giá thấp nhất</th>
                                            <td>{{formatCurrency($hotel['priceMax'])}}</td>
                                        </tr>
                                        <tr>
                                            <th>Sao</th>
                                            <td>
                                                @for($i = 0; $i < $hotel['starRate']; $i++)
                                                    <i class="fa fa-star yellow"></i>
                                                @endfor
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Sao</th>
                                            <td>
                                                <input type="hidden" class="form-control" name="maps_address" id="maps_address" value="" placeholder="Nhập tên địa điểm cần tìm">
                                                <div id="maps_maparea">
                                                    <div id="maps_mapcanvas" style="margin-top:10px; width: 100%!important;" class="form-group"></div>
                                                    <div class="row hidden">
                                                        <div class="col-xs-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">L</span>
                                                                    <input type="text" class="form-control" name="maps[maps_mapcenterlat]" id="maps_mapcenterlat" value="{{$hotel['latitude']}}" readonly="readonly">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">N</span>
                                                                    <input type="text" class="form-control" name="maps[maps_mapcenterlng]" id="maps_mapcenterlng" value="{{$hotel['longitude']}}" readonly="readonly">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">L</span>
                                                                    <input type="text" class="form-control" name="maps[maps_maplat]" id="maps_maplat" value="{{$hotel['latitude']}}"readonly="readonly">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-6">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">N</span>
                                                                    <input type="text" class="form-control" name="maps[maps_maplng]" id="maps_maplng" value="{{$hotel['longitude']}}" readonly="readonly">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row hidden">
                                                        <div class="col-xs-12">
                                                            <div class="form-group">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon">Z</span>
                                                                    <input type="text" class="form-control" name="maps[maps_mapzoom]" id="maps_mapzoom" value="15" readonly="readonly">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{route('admin-categories.hotel.index')}}" class="btn btn-primary">Quay lại</a>
                            <a href="{{route('admin-categories.hotel.edit', ['id' => $hotel['hotelId']])}}" class="btn btn-primary">Sửa</a>
                            {{--<button type="button" class="btn btn-danger hotel-delete" data-id="{{$hotel['hotelId']}}" data-hotel="{{$hotel['name']}}">Xóa</button>--}}
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/adminCategories/hotel.js')}}"></script>
@endpush