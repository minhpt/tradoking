@extends('admin::layouts.master')
@section('title')
    Danh sách khách sạn
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('modules/AdminUsers/css/user.css')}}">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                khách sạn
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">khách sạn</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sửa thông tin khách sạn</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form method="post" action="{{route('admin-categories.hotel.edit',['id' => $hotel->getValue()['hotelId']])}}" class="form-horizontal validation-form">
                            {{csrf_field()}}
                            @include('admincategories::hotel._form')
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info">Chỉnh sửa</button>
                                <button type="submit" class="btn btn-default">Hủy bỏ</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop