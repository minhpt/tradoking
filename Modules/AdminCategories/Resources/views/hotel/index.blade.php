@extends('admin::layouts.master')
@section('title')
    Danh sách khách sạn
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Khách sạn
            </h1>
            <a href="{{route('admin-categories.hotel.create')}}" class="btn btn-primary btn-box-above">Tạo khách sạn</a>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Khách sạn</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title col-md-2">Danh sách khách sạn</h3>
                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" id="nav-search-input" class="form-control pull-right"
                                           placeholder="Search">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @include('message')
                            @include('hard_message')
                            <table class="table table-hover results-table" id="hotels" data-url="/admin/hotels/pagination">
                                <thead>
                                <tr>
                                    <th class="order-number">STT</th>
                                    <th>Tên</th>
                                    <th>Địa chỉ</th>
                                    <th>Giá cao nhất</th>
                                    <th>Giá thấp nhất</th>
                                    <th class="col-md-1">Sao</th>
                                    <th class="group-action-3"></th>
                                </tr>
                                </thead>
                                <tbody>

                                @if(count($hotels) > 0 )
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($hotels as $hotel)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$hotel['name']}}</td>
                                            <td>{{$hotel['address']}}</td>
                                            <td>{{formatCurrency($hotel['priceMax'])}}</td>
                                            <td>{{formatCurrency($hotel['priceMin'])}}</td>
                                            <td>
                                                @for($k = 0; $k < $hotel['starRate']; $k++)
                                                    <i class="fa fa-star yellow"></i>
                                                @endfor
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="{{route('admin-categories.hotel.detail', ['id' => $hotel['hotelId']])}}"
                                                       class="btn btn-success"><i class="fa fa-eye"></i></a>
                                                    <a href="{{route('admin-categories.hotel.edit', ['id' => $hotel['hotelId']])}}"
                                                       class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                                    <button type="button" class="btn btn-danger hotel-delete" data-id="{{$hotel['hotelId']}}" data-hotel="{{$hotel['name']}}"><i
                                                                class="fa fa-trash-o"></i></button>
                                                </div>
                                            </td>
                                            @php
                                                $i++;
                                            @endphp
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">Không có dữ liệu</td>
                                    </tr>
                                @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="col-md-6 widget-page">
                                @include('admin::pagination.index',['current_page' => 1,'total_page' => $pages])
                            </div>
                            <div class="col-md-6 hide">
                                <select class="form-control pull-right" id="show-records">
                                    <option>10</option>
                                    <option>20</option>
                                    <option>50</option>
                                    <option>100</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@stop
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/adminCategories/hotel.js')}}"></script>
    <script src="{{asset('modules/common/js/pagination-search.js')}}"></script>
    {{--<script>--}}
        {{--activeMenu('categories', 'hotel', true);--}}
    {{--</script>--}}
@endpush
