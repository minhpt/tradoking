@if(count($hotels) > 0 )
    @php
        $i = 1;
    @endphp
    @foreach($hotels as $hotel)
        <tr>
            <td>{{$i}}</td>
            <td>{{$hotel['name']}}</td>
            <td>{{$hotel['address']}}</td>
            <td>{{formatCurrency($hotel['priceMax'])}}</td>
            <td>{{formatCurrency($hotel['priceMin'])}}</td>
            <td>
                @for($k = 0; $k < $hotel['starRate']; $k++)
                    <i class="fa fa-star yellow"></i>
            @endfor
            <td>
                <div class="btn-group btn-group-xs">
                    <a href="{{route('admin-categories.hotel.detail', ['id' => $hotel['hotelId']])}}"
                       class="btn btn-success"><i class="fa fa-eye"></i></a>
                    <a href="{{route('admin-categories.hotel.edit', ['id' => $hotel['hotelId']])}}"
                       class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <button type="button" class="btn btn-danger hotel-delete" data-id="{{$hotel['hotelId']}}" data-hotel="{{$hotel['name']}}"><i
                                class="fa fa-trash-o"></i></button>
                </div>
            </td>
            @php
                $i++;
            @endphp
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="7">Không có dữ liệu</td>
    </tr>
@endif
<input id="total-pages-current" type="hidden" value="{{ isset($pages) ? $pages : 0 }}">