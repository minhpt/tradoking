@extends('admin::layouts.master')
@section('title')
    Danh sách Loại Phòng
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('modules/AdminUsers/css/user.css')}}">
@stop
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Loại Phòng
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Loại Phòng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sửa thông tin Loại Phòng</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form method="post" action="{{route('admin-categories.type.edit',['id' => $id])}}" class="form-horizontal">
                        {{csrf_field()}}
                        @include('admincategories::type._form')
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info">Chỉnh sửa</button>
                                <button type="submit" class="btn btn-default">Hủy bỏ</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop