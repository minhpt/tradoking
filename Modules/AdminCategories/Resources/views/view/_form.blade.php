@php
    if(isset($view))
        $view = $view->getValue();
@endphp
<div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-3 control-label">Hướng nhìn @include('require')</label>

                <div class="col-md-4">
                    <input type="text" class="form-control" id="view" name="{{isset($id) ? $id : null}}" value="{{isset($view) ? $view : null}}">
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/AdminSystem/user.js')}}"></script>
@endpush