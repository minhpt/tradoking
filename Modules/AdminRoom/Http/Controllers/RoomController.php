<?php

namespace Modules\AdminRoom\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    private $db;
    private $storage;
    private $object;
    private $hotels = [];
    private $hotelBenefits = [];
    private $typeOfRooms = [];
    private $views = [];
    private $pageKey;
    private $records;

    public function __construct()
    {
        $this->db = dbFireBase()->getDatabase();
        $this->storage = dbFireBase()->getStorage();
        $this->object = 'products';
        $this->hotelBenefits = arrayPush($this->db->getReference('dataInfo/hotelBenefits')->getSnapshot());
        $this->typeOfRooms = arrayPush($this->db->getReference('dataInfo/typeOfRooms')->getSnapshot());
        $this->views = arrayPush($this->db->getReference('dataInfo/views')->getSnapshot());
        $this->hotels = arrayPush($this->db->getReference('hotels')->getSnapshot());
        $this->records = 10;
        $this->pageKey = pageKey($this->db->getReference($this->object)->getValue(), $this->records);
    }

    public function index()
    {
        $products = $this->db->getReference($this->object)->orderByKey()->limitToFirst($this->records)->getSnapshot()->getValue();
        $pages = self::getPages();
        return view('adminroom::index',compact('products','pages'));
    }

    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Get record by page number
     */
    public function pagination($currentPage, $records, $search = null)
    {
        $products = $this->db->getReference($this->object)->orderByKey()->startAt($this->pageKey[$currentPage-1])->limitToFirst($this->records)->getValue();
        $pages = self::getPages();
        return view('adminroom::pagination', compact('pages', 'products'));
    }

    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Get total page
     */
    public function getPages()
    {
        return ceil($this->db->getReference($this->object)->getSnapshot()->numChildren()/$this->records);
    }

    public function create(Request $request)
    {
        if($request->isMethod('GET'))
        {
            return view('adminroom::create',
                [
                    'hotels'    => $this->hotels,
                    'roomTypes' => $this->typeOfRooms,
                    'views'     => $this->views
                ]);
        }
        //create user
        $user = session('user');
        if(!isset($user['userId']))
        {
            $user['fullName'] = $user['name']['first'] . ' ' . $user['name']['last'];
//            $user['address'] = null;
//            $user['birthday'] = null;
            $user['createTime'] = strtotime(date('Y-m-d H:i:s'));
//            $user['gender'] = null;
//            $user['identification'] = null;
            $user['nationality'] = "Viet Nam";
            $user['personalAccountNumber'] = "Viet Nam";
            $user['userStatus'] = 0;
            $user['userType'] = 0;
            $staffId = $user['staffId'];
            unset($user['name'], $user['staffId']);
            $keyUser = $this->db->getReference('users')->push($user)->getKey();
            $this->db->getReference('users/'.$keyUser)->update(['userId' => $keyUser])->getKey();

            $this->db->getReference('staffs/' . $staffId)->update(['userId' => $keyUser]);

            $user['userId'] = $keyUser;
            session(['user' => $this->db->getReference('staffs/' . $staffId)->getValue()]);
        }
        $params = $request->all();
        array_shift($params);
        $strDates = null;
        if(isset($params['dates']))
        {
            $dates = $params['dates'];
            $types = $params['types'];
            $values = $params['values'];
            if (!empty($dates)) {
                $arrayPrice = [];
                foreach ($dates as $key => $date) {
                    $arrayDate = explode(' - ', $date);
                    $tmpDate = $date;
                    if ($arrayDate[0] === $arrayDate[1]) {
                        $tmpDate = $arrayDate[0];
                    }
                    $strDates .= ',' . $tmpDate;
                    $tmp = [
                        'date'  => $tmpDate,
                        'type'  => $types[$key],
                        'value' => $values[$key],
                        'cost'  => $this->calculatorCost($values[$key], $types[$key], $params['priceDefault'])
                    ];
                    array_push($arrayPrice, (object)($tmp));
                }
                $params['prices'] = $arrayPrice;
            }
        }
        $strDates = trim($strDates, ',');
        $params['dateOfUsed'] = $strDates;
        $params['lastUpdate'] = strtotime(date('Y-m-d H:i:s'));
        if(isset($params['images']))
        {
            $images = $params['images'];
            $sizes = $params['imageSizes'];
            $names = $params['imageNames'];
            if (!empty($images)) {
                $arrayImages = [];
                foreach ($images as $key => $image) {
                    $tmp = [
                        'path'  => $image,
                        'size'  => $sizes[$key],
                        'name' => $names[$key],
                    ];
                    array_push($arrayImages, (object)($tmp));
                }
                $params['images'] = $arrayImages;
            }
        }
        if(isset($params['documents']))
        {
            $images = $params['documents'];
            $sizes = $params['documentSizes'];
            $names = $params['documentNames'];
            if (!empty($images)) {
                $arrayDocuments = [];
                foreach ($images as $key => $image) {
                    $tmp = [
                        'path'  => $image,
                        'size'  => $sizes[$key],
                        'name' => $names[$key],
                    ];
                    array_push($arrayDocuments, (object)($tmp));
                }
                $params['documents'] = $arrayDocuments;
            }
        }
        $key = array_search($params['hotelId'], array_column($this->hotels, 'hotelId'));
        $params['hotel'] = $this->hotels[$key];
        $user = $this->db->getReference('users/'.$user['userId'])->getValue();
        $params['user'] = [
            'email' => $user['email'],
            'fullName' => $user['fullName'],
            'gender' => isset($user['gender']) ? $user['gender'] : null,
            'nationality' => $user['nationality'],
            'phoneNumber' => $user['phoneNumber'],
            'userId' => $user['userId'],
        ];
        unset($params['documentSizes']);
        unset($params['documentNames']);
        unset($params['imageSizes']);
        unset($params['imageNames']);
        unset($params['types']);
        $params['createTime'] = strtotime(date('Y-m-d H:i:s'));
        $key = $this->db->getReference($this->object)->push($params)->getKey();
        $this->db->getReference($this->object. '/'.$key)->update(['productId' => $key]);
        messageSession($request, 'success', 'Thêm thành công');
        return redirect()->route('admin-room.room.index');
    }
    public function edit(Request $request, $id)
    {
        $product = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
        if ($request->isMethod('GET')) {
            if (!$product->exists()) return abort(404);
            $dates = isset($product->getValue()['dateOfUsed']) ? explode(',', $product->getValue()['dateOfUsed']) : null;
            if (!empty($dates))
                foreach ($dates as $key => $date) {
                    $date = trim($date);
                    $array = explode('-', $date);
                    if (count($array) == 1) $dates[$key] = $date . ' - ' . $date;
                }
            $prices = isset($product->getValue()['prices']) ? $product->getValue()['prices'] : null;
            return view('adminroom::edit',
                [
                    'product'   => $product,
                    'hotels'    => $this->hotels,
                    'roomTypes' => $this->typeOfRooms,
                    'views'     => $this->views,
                    'dates'     => $dates,
                    'prices'    => $prices
                ]);
        }
        $params = $request->all();
        array_shift($params);
        $strDates = null;
        if(isset($params['dates']))
        {
            $dates = $params['dates'];
            $types = $params['types'];
            $values = $params['values'];
            if (!empty($dates)) {
                $arrayPrice = [];
                foreach ($dates as $key => $date) {
                    $arrayDate = explode(' - ', $date);
                    $tmpDate = $date;
                    if ($arrayDate[0] === $arrayDate[1]) {
                        $tmpDate = $arrayDate[0];
                    }
                    $tmp = [
                        'date'  => $tmpDate,
                        'type'  => $types[$key],
                        'value' => $values[$key],
                        'cost'  => $this->calculatorCost($values[$key], $types[$key], $params['priceDefault'])
                    ];
                    array_push($arrayPrice, (object)($tmp));
                }
                $params['prices'] = $arrayPrice;
            }
        }
        if(isset($params['dateOfUsed']))
        {
            $dates = $params['dateOfUsed'];
            if (!empty($dates)) {
                foreach ($dates as $key => $date) {
                    $arrayDate = explode(' - ', $date);
                    $tmpDate = $date;
                    if ($arrayDate[0] === $arrayDate[1]) {
                        $tmpDate = $arrayDate[0];
                    }
                    $strDates .= ',' . $tmpDate;
                }
            }
        }
        $strDates = trim($strDates, ',');
        $params['dateOfUsed'] = $strDates;
        $params['lastUpdate'] = strtotime(date('Y-m-d H:i:s'));
        if(isset($params['images']))
        {
            $images = $params['images'];
            $sizes = $params['imageSizes'];
            $names = $params['imageNames'];
            if (!empty($images)) {
                $arrayImages = [];
                foreach ($images as $key => $image) {
                    $tmp = [
                        'path'  => $image,
                        'size'  => $sizes[$key],
                        'name' => $names[$key],
                    ];
                    array_push($arrayImages, (object)($tmp));
                }
                $params['images'] = $arrayImages;
            }
        }
        if(isset($params['documents']))
        {
            $images = $params['documents'];
            $sizes = $params['documentSizes'];
            $names = $params['documentNames'];
            if (!empty($images)) {
                $arrayDocuments = [];
                foreach ($images as $key => $image) {
                    $tmp = [
                        'path'  => $image,
                        'size'  => $sizes[$key],
                        'name' => $names[$key],
                    ];
                    array_push($arrayDocuments, (object)($tmp));
                }
                $params['documents'] = $arrayDocuments;
            }
        }
        unset($params['documentSizes']);
        unset($params['documentNames']);
        unset($params['imageSizes']);
        unset($params['imageNames']);
        unset($params['types']);
        foreach ($params as $key => $value) {
            $this->db->getReference($this->object . '/' . $id . '/' . $key)->set($value);
        }
        messageSession($request, 'success', 'Cập nhật thành công');
        $products = $this->db->getReference($this->object)->getSnapshot();
        return redirect()->route('admin-room.room.index');
    }

    public function detail($id)
    {
        return view('adminroom::detail');
    }

    private function calculatorCost($value, $type, $priceDefault)
    {
        switch ($type) {
            case 2:
                $cost = $priceDefault - $value;
                break;
            case 3:
                $cost = (float)$value;
                break;
            case 4:
                $cost = $priceDefault * $value / 100 + $priceDefault;
                break;
            case 5:
                $cost = $priceDefault - $priceDefault * $value / 100;
                break;
            default:
                $cost = $priceDefault + $value;
                break;
        }
        return $cost;
    }
    public function getFilesByRoomId($id)
    {
        $product = $this->db->getReference($this->object . '/' . $id)->getSnapshot()->getValue();
        $images = isset($product['images']) ? $product['images'] : null;
        $documents = isset($product['documents']) ? $product['documents'] : null;
        return response()->json(['images' => $images,'documents' => $documents]);
    }
    public function delete($id)
    {
        $hotel = $this->db->getReference($this->object . '/' . $id)->remove();
        return response()->json($hotel);
    }
}