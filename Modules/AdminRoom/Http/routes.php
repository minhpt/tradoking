<?php

Route::group(['middleware' => ['web','auth.ptm'], 'prefix' => 'admin/room', 'namespace' => 'Modules\AdminRoom\Http\Controllers'], function()
{
    Route::get('/', 'RoomController@index')->name('admin-room.room.index');
    Route::get('/create', 'RoomController@create')->name('admin-room.room.create');
    Route::post('/create', 'RoomController@create')->name('admin-room.room.create');
    Route::get('/edit/{id}', 'RoomController@edit')->name('admin-room.room.edit');
    Route::post('/edit/{id}', 'RoomController@edit')->name('admin-room.room.edit');
    Route::get('/detail/{id}', 'RoomController@detail')->name('admin-room.room.detail');
    Route::get('/delete/{id}', 'RoomController@delete')->name('admin-room.room.delete');
    Route::get('/get-files/{id}', 'RoomController@getFilesByRoomId')->name('admin-room.room.get-files');

    //Pagination
    Route::get('/pagination/{currentPage}/{records}/{search?}', 'RoomController@pagination')->name('admin-room.room.pagination');
});
