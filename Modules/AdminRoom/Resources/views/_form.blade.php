@php
    if(isset($product))
    {
        $product = $product->getValue();
    }
@endphp
@push('style')
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/select2/select2.css')}}">
@endpush
<div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-3 control-label">Khách sạn @include('require')</label>

                <div class="col-md-4">
                    <select class="form-control" id="hotels" name="hotelId">
                        @if(!empty($hotels))
                            @foreach($hotels as $hotel)
                                <option value="{{$hotel['hotelId']}}" data-address="{{$hotel['address']}}" data-latitude="{{$hotel['latitude']}}" data-longitude="{{$hotel['longitude']}}"
                                        @isset($product) @if(trim($product['hotel']['hotelId']) == trim($hotel['hotelId'])) selected @endif @endisset>{{trim($hotel['name'])}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Địa chỉ</label>

                <div class="col-md-6">
                    <label class="address control-label">aa</label>
                    <input class="latitude" name="latitude" type="hidden" value="">
                    <input class="longitude" name="longitude" type="hidden" value="">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Số phòng @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="nameOfRoom" name="nameOfRoom"
                               value="@isset($product){{trim($product['nameOfRoom'])}}@endisset">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Loại Phòng @include('require')</label>

                <div class="col-md-4">
                    <select class="form-control" name="kindOfRoom">
                        @if(!empty($roomTypes))
                            @foreach($roomTypes as $roomType)
                                <option value="{{trim($roomType)}}"
                                        @isset($product) @if(trim($product['kindOfRoom']) == trim($roomType)) selected @endif @endisset>{{trim($roomType)}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Phòng ngủ @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="number" class="form-control" id="numberOfBedroom" name="numberOfBedroom"
                           value="@isset($product['numberOfBedroom']){{trim($product['numberOfBedroom'])}}@endisset">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Số người @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="number" class="form-control" id="numberOfPeople" name="numberOfPeople"
                           value="@isset($product){{trim($product['numberOfPeople'])}}@endisset">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Giá @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <div class="input-group">

                            <input type="number" class="form-control" id="priceDefault" name="priceDefault"
                                   value="@isset($product){{trim($product['priceDefault'])}}@endisset">
                            <span class="input-group-btn">
                            <a href="#" id="add-cost" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8">
                    <table class="table table-striped" id="table-room-costs">
                        <thead>
                        <th>Thời gian</th>
                        <th>Lựa chọn</th>
                        <th style="width: 100px">Giá trị</th>
                        <th style="width: 50px"></th>
                        </thead>
                        <tbody>
                        @isset($product['prices'])
                            @foreach($prices as $item)
                                <tr>
                                    <td>
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-clock-o"></i>
                                            </div>
                                            <input type="text" class="form-control pull-right reservationtime"
                                                   name="dates[]" value="{{$item['date']}}">
                                        </div>
                                    </td>
                                    <td>
                                        <select class="form-control" name="types[]">
                                            @foreach(\App\Constantine::Types as $key => $type)
                                                <option value="{{$key}}"
                                                        @if($item['type'] == $key) selected @endif>{{$type}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <input type="number" class="form-control" value="{{$item['value']}}"
                                               name="values[]">
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-danger remove-cost"><i
                                                    class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endisset
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Số ngày @include('require')</label>

                <div class="col-md-4">
                    <div class="input-group">
                        <label class="form-control times">{{isset($product['numberDateOfUsed']) ? $product['numberDateOfUsed'] : 0}}</label>
                        <input type="hidden" id="times" name="numberDateOfUsed"
                               value="{{isset($product['numberDateOfUsed']) ? $product['numberDateOfUsed'] : 0}}"/>
                        <span class="input-group-btn">
                        <a href="#" id="add-time" class="btn btn-primary"><i class="fa fa-plus"></i></a>
                    </span>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8">
                    <table class="table table-striped" id="table-room-times">
                        <thead>
                        <th>Thời gian</th>
                        <th style="width: 50px"></th>
                        </thead>
                        <tbody>
                        @if(!empty($dates))
                            @foreach($dates as $date)
                                <tr>
                                    <td><input type="text" class="form-control available-date" name="dateOfUsed[]"
                                               value="{{$date}}"></td>
                                    <td>
                                        <button type="button" class="btn btn-danger remove-time"><i
                                                    class="fa fa-times"></i>
                                        </button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="inputPassword3" class="col-md-2 control-label">Hướng nhìn</label>

                <div class="col-md-5">
                    <input type="hidden" value="@isset($product['views']) @json($product['views']) @endisset"
                           id="selected-views">
                    <select class="form-control view-multiple" multiple="multiple" data-placeholder="Select a State"
                            name="views[]"
                            style="width: 100%;">
                        @foreach($views as $view)
                            <option {{isset($product['views']) ? (array_search($view, $product['views'], true) !== false ? 'selected' : null) : null}}>{{$view}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-md-2 control-label">Hình ảnh phòng</label>

                <div class="col-md-5 images">
                    <div id="upload-images-room" class="dropzone"></div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputEmail3" class="col-md-2 control-label">Tài liệu chứng thực</label>

                <div class="col-md-5 documents">
                    <input type="hidden" value="@isset($product['productId']){{$product['productId']}}@endisset" id="productId">
                    <div id="upload-documents-room" class="dropzone with-100-precent"></div>
                </div>
            </div>
            {{--<div class="form-group">--}}
                {{--<label for="inputPassword3" class="col-md-2 control-label">Mô tả</label>--}}

                {{--<div class="col-md-4">--}}
                    {{--<textarea rows="5" class="with-100-precent"></textarea>--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group">
                <label for="inputPassword3" class="col-md-2 control-label">Trạng thái</label>

                <div class="col-md-4">
                    <select class="form-control" name="status">
                        <option value="1">Xuất bản</option>
                        <option value="0">Gỡ bài</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script src="{{asset('/modules/common/js/firebase.js')}}"></script>
    <!-- DataTables -->
    <script src="{{asset('assets/admin/plugins/dropzone/dropzone.min.js')}}"></script>
    <script src="{{asset('modules/adminRoom/js/room-no-index.js')}}"></script>
    <script src="{{asset('modules/adminRoom/js/room-validate.js')}}"></script>
    <!-- Select2 -->
    <script src="{{asset('assets/admin/plugins/select2/select2.full.js')}}"></script>
@endpush