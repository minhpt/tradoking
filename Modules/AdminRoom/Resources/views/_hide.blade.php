<table id="add-row-table-cost" class="hide">
    <tr>
        <td>
            <div class="input-group">
                <div class="input-group-addon">
                    <i class="fa fa-clock-o"></i>
                </div>
                <input type="text" class="form-control pull-right reservationtime" name="dates[]">
            </div>
        </td>
        <td>
            <select class="form-control" name="types[]">
                @foreach(\App\Constantine::Types as $key => $type)
                    <option value="{{$key}}">{{$type}}</option>
                @endforeach
            </select>
        </td>
        <td>
            <input type="number" class="form-control" value="0" name="values[]">
        </td>
        <td>
            <button type="button" class="btn btn-danger remove-cost"><i class="fa fa-times"></i>
            </button>
        </td>
    </tr>
</table>
<table id="add-row-table-time" class="hide">
    <tr>
        <td><input type="text" class="form-control available-date" name="dateOfUsed[]"></td>
        <td>
            <button type="button" class="btn btn-danger remove-time"><i class="fa fa-times"></i>
            </button>
        </td>
    </tr>
</table>