@extends('admin::layouts.master')
@section('title')
    Tạo phòng
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('modules/AdminUsers/css/user.css')}}">
@stop
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Phòng
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Phòng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thêm thông tin phòng</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <form method="post" action="{{route('admin-room.room.create')}}" class="form-horizontal validation-form">
                        {{csrf_field()}}
                            <input type="hidden" value="1" id="action">
                            @include('adminroom::_form')
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-info">Tạo</button>
                                <button type="submit" class="btn btn-default">Hủy bỏ</button>
                            </div>
                            <!-- /.box-footer -->
                        </form>
                        @include('adminroom::_hide')
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop