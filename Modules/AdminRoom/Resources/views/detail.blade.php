@extends('admin::layouts.master')
@section('title')
    Chi tiết phòng
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/bootstrap-year-calendar/bootstrap-year-calendar.css')}}">
@stop
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Phòng
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Phòng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin phòng</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-offset-3">
                                    <table class="table table-bordered table-detail">
                                        <tr>
                                            <th class="col-md-6">Ảnh phòng</th>
                                            <td class="col-md-6"><img class="thumbnail" style="width: 35%"
                                                                      src="{{asset('images/defaults/No_Image_Available.jpg')}}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Khách sạn</th>
                                            <td>Mường Thanh</td>
                                        </tr>
                                        <tr>
                                            <th>Địa chỉ</th>
                                            <td>Blablabla</td>
                                        </tr>
                                        <tr>
                                            <th>Số phòng</th>
                                            <td>3001</td>
                                        </tr>
                                        <tr>
                                            <th>Loại Phòng</th>
                                            <td>Bá đạo</td>
                                        </tr>
                                        <tr>
                                            <th>Phòng ngủ</th>
                                            <td>2</td>
                                        </tr>
                                        <tr>
                                            <th>Người</th>
                                            <td>3</td>
                                        </tr>
                                        <tr>    
                                            <th>Giá</th>
                                            <td>
                                                <div class="form-group">
                                                   <label>Giá mặc định: $300.00</label>
                                                </div>
                                                <div class="form-group">
                                                    <label>Giá khác: </label>
                                                    <div class="col-md-12">
                                                        <table class="table table-striped" id="table-room-costs">
                                                            <thead>
                                                            <th>Từ ngày</th>
                                                            <th>Đến ngày</th>
                                                            <th style="width: 100px">Giá</th>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td>01/01/2018</td>
                                                                <td>01/03/2018</td>
                                                                <td>
                                                                    $350.00
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Thời gian còn lại</th>
                                            <td>
                                                30 ngày
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Mô tả</th>
                                            <td>Việt Nam Đẹp vcc</td>
                                        </tr>
                                        <tr>
                                            <th>Trạng thái</th>
                                            <td>Xuất bản</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{route('admin-users.user.edit', ['id' => 1])}}" class="btn btn-primary">Sửa</a>
                            <button type="button" class="btn btn-danger delete-user">Xóa</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('assets/admin/plugins/bootstrap-year-calendar/bootstrap-year-calendar.js')}}"></script>
    <script src="{{asset('modules/adminRoom/js/room.js')}}"></script>
@endpush