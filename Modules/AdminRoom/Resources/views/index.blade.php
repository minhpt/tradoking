@extends('admin::layouts.master')
@section('title')
    Danh sách phòng
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/bootstrap-year-calendar/bootstrap-year-calendar.css')}}">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Phòng
            </h1>
            <a href="{{route('admin-room.room.create')}}" class="btn btn-primary btn-box-above">Tạo phòng</a>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Phòng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title col-md-2">Danh sách người dùng</h3>
                            <div class="widget-filter col-md-6 text-center">
                                <span class="filter-name">
                                    Bộ lọc:
                                </span>
                                <span class="filter-name">
                                    <a href="#"><i class="fa fa-circle red"></i> Đã duyệt</a>
                                </span>
                                <span class="filter-name">
                                    <a href="#"><i class="fa fa-circle yellow"></i> Đang chờ</a>
                                </span>
                            </div>
                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" id="nav-search-input"  class="form-control pull-right" id="search-datatable"
                                           placeholder="Search">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table table-striped table-bordered results-table" id="rooms" data-url="/admin/room/pagination">
                                <thead>
                                <tr>
                                    <th class="order-number">STT</th>
                                    <th>Khách sạn</th>
                                    <th>Địa chỉ</th>
                                    <th>Số phòng</th>
                                    <th>Đêm cần bán</th>
                                    <th>Loại phòng</th>
                                    <th>Phòng ngủ</th>
                                    <th>Số người</th>
                                    <th>Giá</th>
                                    <th>Trạng thái</th>
                                    <th class="group-action-3"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($products) > 0)
                                    @php
                                        $i = 1;
                                        $dates = isset($product['DateOfUsed']) ? $product['DateOfUsed'] : null;
                                    @endphp
                                    @foreach($products as $product)
                                        <tr>
                                            <td>{{$i}}</td>
                                            @if(isset($product['hotel']))
                                                <td>
                                                    <div>{{$product['hotel']['name']}}</div>
                                                    <div>
                                                        @for($k = 0; $k < $product['hotel']['starRate']; $k++)
                                                            <i class="fa fa-star yellow"></i>
                                                        @endfor
                                                    </div>
                                                </td>
                                                <td>{{$product['hotel']['address']}}</td>
                                            @else
                                                <td></td>
                                                <td></td>
                                            @endif
                                            <td>{{$product['nameOfRoom']}}</td>
                                            <td>{{isset($product['numberDateOfUsed']) ? $product['numberDateOfUsed'] : 0}} <a href="#"><i
                                                            class="fa fa-angle-double-up blue detail-night"></i></a>
                                            </td>
                                            <td>{{$product['kindOfRoom']}}</td>
                                            <td>{{isset($product['numberOfBedroom']) ? $product['numberOfBedroom'] : null}}</td>
                                            <td>{{$product['numberOfPeople']}}</td>
                                            <td>{{formatCurrency($product['priceDefault'])}}</td>
                                            <td><i class="fa fa-circle red"></i></td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    {{--<a href="{{route('admin-room.room.detail', ['id' => $product['productId']])}}"--}}
                                                       {{--class="btn btn-success"><i class="fa fa-eye"></i></a>--}}
                                                    <a href="{{route('admin-room.room.edit', ['id' => $product['productId']])}}"
                                                       class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                                    <button type="button" class="btn btn-danger room-delete" data-room="{{$product['nameOfRoom'] . ' - ' . $product['hotel']['name']}}" data-room-id="{{$product['productId']}}"><i
                                                                class="fa fa-trash-o"></i></button>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="detail-calendar" style="display: none">
                                            <td colspan="11">
                                                <div id="calendar{{$i}}" class="calendar"></div>
                                            </td>
                                            <td style="display: none"></td>
                                            <td style="display: none"></td>
                                            <td style="display: none"></td>
                                            <td style="display: none"></td>
                                            <td style="display: none"></td>
                                            <td style="display: none"></td>
                                            <td style="display: none"></td>
                                            <td style="display: none"></td>
                                            <td style="display: none"></td>
                                            <td style="display: none"></td>
                                        </tr>
                                        @php
                                            $i++;
                                        @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">Không có dữ liệu</td>
                                    </tr>
                                @endif

                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="col-md-6 widget-page">
                                @include('admin::pagination.index',['current_page' => 1,'total_page' => $pages])
                            </div>
                            <div class="col-md-6 hide">
                                <select class="form-control pull-right" id="show-records">
                                    <option>10</option>
                                    <option>20</option>
                                    <option>50</option>
                                    <option>100</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@stop
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/common/js/pagination-search.js')}}"></script>
    <script src="{{asset('modules/adminRoom/js/room.js')}}"></script>
    <script src="{{asset('modules/adminRoom/js/room-no-index.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap-year-calendar/bootstrap-year-calendar.js')}}"></script>
@endpush
