@if(count($products) > 0)
    @php
        $i = 1;
        $dates = isset($product['DateOfUsed']) ? $product['DateOfUsed'] : null;
    @endphp
    @foreach($products as $product)
    <tr>
        <td>{{$i}}</td>
        @if(isset($product['hotel']))
            <td>
                <div>{{$product['hotel']['name']}}</div>
                <div>
                    @for($k = 0; $k < $product['hotel']['starRate']; $k++)
                        <i class="fa fa-star yellow"></i>
                    @endfor
                </div>
            </td>
            <td>{{$product['hotel']['address']}}</td>
        @else
            <td></td>
            <td></td>
        @endif
        <td>{{$product['nameOfRoom']}}</td>
        <td>{{isset($product['numberDateOfUsed']) ? $product['numberDateOfUsed'] : 0}} <a href="#"><i
                        class="fa fa-angle-double-up blue detail-night"></i></a>
        </td>
        <td>{{$product['kindOfRoom']}}</td>
        <td>{{isset($product['numberOfBedroom']) ? $product['numberOfBedroom'] : null}}</td>
        <td>{{$product['numberOfPeople']}}</td>
        <td>{{formatCurrency($product['priceDefault'])}}</td>
        <td><i class="fa fa-circle red"></i></td>
        <td>
            <div class="btn-group btn-group-xs">
                {{--<a href="{{route('admin-room.room.detail', ['id' => $product['productId']])}}"--}}
                {{--class="btn btn-success"><i class="fa fa-eye"></i></a>--}}
                <a href="{{route('admin-room.room.edit', ['id' => $product['productId']])}}"
                   class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                <button type="button" class="btn btn-danger room-delete"
                        data-room="{{$product['nameOfRoom'] . ' - ' . $product['hotel']['name']}}"
                        data-room-id="{{$product['productId']}}"><i
                            class="fa fa-trash-o"></i></button>
            </div>
        </td>
    </tr>
    <tr class="detail-calendar" style="display: none">
        <td colspan="11">
            <div id="calendar{{$i}}" class="calendar"></div>
        </td>
        <td style="display: none"></td>
        <td style="display: none"></td>
        <td style="display: none"></td>
        <td style="display: none"></td>
        <td style="display: none"></td>
        <td style="display: none"></td>
        <td style="display: none"></td>
        <td style="display: none"></td>
        <td style="display: none"></td>
        <td style="display: none"></td>
    </tr>
    @php
        $i++;
    @endphp
    @endforeach
@else
    <tr>
        <td colspan="7">Không có dữ liệu</td>
    </tr>
@endif
<input id="total-pages-current" type="hidden" value="{{ isset($pages) ? $pages : 0 }}">