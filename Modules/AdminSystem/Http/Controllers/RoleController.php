<?php

namespace Modules\AdminSystem\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('adminsystem::role.index');
    }
    public function create()
    {
        return view('adminsystem::role.create');
    }
    public function edit($id)
    {
        return view('adminsystem::role.edit');
    }
    public function detail($id)
    {
        return view('adminsystem::role.detail');
    }
}
