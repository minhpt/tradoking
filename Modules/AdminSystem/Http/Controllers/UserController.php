<?php

namespace Modules\AdminSystem\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('adminsystem::user.index');
    }
    public function create()
    {
        return view('adminsystem::user.create');
    }
    public function edit($id)
    {
        return view('adminsystem::user.edit');
    }
    public function detail($id)
    {
        return view('adminsystem::user.detail');
    }
}
