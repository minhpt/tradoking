<?php

Route::group(['middleware' => ['web','auth.ptm'], 'prefix' => 'admin', 'namespace' => 'Modules\AdminSystem\Http\Controllers'], function()
{
    Route::group(['prefix' => 'user-admin'], function(){
        Route::get('/', 'UserController@index')->name('admin-system.user.index');
        Route::get('/create', 'UserController@create')->name('admin-system.user.create');
        Route::get('/edit/{id}', 'UserController@edit')->name('admin-system.user.edit');
        Route::get('/detail/{id}', 'UserController@detail')->name('admin-system.user.detail');
    });
    Route::group(['prefix' => 'role'], function(){
        Route::get('/', 'RoleController@index')->name('admin-system.role.index');
        Route::get('/create', 'RoleController@create')->name('admin-system.role.create');
        Route::get('/edit/{id}', 'RoleController@edit')->name('admin-system.role.edit');
        Route::get('/detail/{id}', 'RoleController@detail')->name('admin-system.role.detail');
    });

});
