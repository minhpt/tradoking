<form class="form-horizontal">
<div class="row">
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Sửa thông tin quyền</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="inputEmail3" class="col-md-3 control-label">Tên @include('require')</label>

                            <div class="col-md-7">
                                <input type="text" class="form-control" id="inputEmail3">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info">Chỉnh sửa</button>
                <button type="submit" class="btn btn-default">Hủy bỏ</button>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Thông tin chức năng</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12">
                            <label>
                                <input type="checkbox" class="flat-red" checked>
                            </label>
                            <label>
                                Quyền mua bán
                            </label>
                            </div><div class="col-md-12">
                            <label>
                                <input type="checkbox" class="flat-red" checked>
                            </label>
                            <label>
                                Quyền trao đổi
                            </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
</div>

</form>
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/AdminSystem/role.js')}}"></script>
@endpush