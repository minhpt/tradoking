@extends('admin::layouts.master')
@section('title')
    Danh sách Quyền
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('modules/AdminUsers/css/user.css')}}">
@stop
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               Quyền
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Quyền</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin Quyền</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-offset-3">
                                    <table class="table table-bordered table-detail">
                                        <tr>
                                            <th>Tên</th>
                                            <td>Quyền mua bán</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{route('admin-system.user.edit', ['id' => 1])}}" class="btn btn-primary">Sửa</a>
                            <button type="button" class="btn btn-danger delete-user">Xóa</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/AdminSystem/role.js')}}"></script>
@endpush