@extends('admin::layouts.master')
@section('title')
    Danh sách quyền
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('modules/AdminUsers/css/user.css')}}">
@stop
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Quyền
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Quyền</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    @include('adminsystem::role._form')
                </div>
            </div>
        </section>
    </div>
@stop