@extends('admin::layouts.master')
@section('title')
    Danh sách quyền
@endsection
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Quyền
            </h1>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Quyền</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title col-md-2">Danh sách Quyền</h3>

                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th class="order-number">STT</th>
                                    <th>Tên</th>
                                    <th class="group-action-3"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Quyền mua bán</td>
                                    <td>
                                        <div class="btn-group btn-group-xs">
                                            <a href="{{route('admin-system.role.detail', ['id' => 1])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('admin-system.role.edit', ['id' => 1])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                            <button type="button" class="btn btn-danger delete-user"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>Quyền trao đổi</td>
                                    <td>
                                        <div class="btn-group btn-group-xs">
                                            <a href="{{route('admin-system.role.detail', ['id' => 1])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('admin-system.role.edit', ['id' => 1])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                            <button type="button" class="btn btn-danger delete-user"><i class="fa fa-trash-o"></i></button>
                                        </div>
                                    </td>
                                </tr>

                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="col-md-6">
                                <ul class="pagination pagination-sm no-margin pull-left">
                                    <li><a href="#">&laquo;</a></li>
                                    <li><a href="#">1</a></li>
                                    <li><a href="#">2</a></li>
                                    <li><a href="#">3</a></li>
                                    <li><a href="#">&raquo;</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <select class="form-control pull-right" id="view-by">
                                    <option>10</option>
                                    <option>20</option>
                                    <option>50</option>
                                    <option>100</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@stop
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/AdminSystem/role.js')}}"></script>
@endpush
