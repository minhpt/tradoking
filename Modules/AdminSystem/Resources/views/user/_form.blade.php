<div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-3 control-label">Họ và tên @include('require')</label>

                <div class="col-md-4">
                    <input type="email" class="form-control" id="inputEmail3">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Số điện thoại @include('require')</label>

                <div class="col-md-4">
                    <input type="text" class="form-control" id="inputPassword3">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Email @include('require')</label>

                <div class="col-md-4">
                    <input type="text" class="form-control" id="inputPassword3">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Quyền @include('require')</label>

                <div class="col-md-4">
                    <select class="form-control with-100-precent" name="states[]">
                        <option value="01">Quyền mua bán</option>
                        <option value="02">Quyền trao đổi</option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/AdminSystem/user.js')}}"></script>
@endpush