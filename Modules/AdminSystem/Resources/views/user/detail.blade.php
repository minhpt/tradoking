@extends('admin::layouts.master')
@section('title')
    Danh sách nhân viên
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('modules/AdminUsers/css/user.css')}}">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               Nhân viên
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Nhân viên</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin nhân viên</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-offset-3">
                                    <table class="table table-bordered table-detail">
                                        <tr>
                                            <th>Họ và tên</th>
                                            <td>Phạm Thanh Minh</td>
                                        </tr>
                                        <tr>
                                            <th>Số điện thoại</th>
                                            <td>01667862346</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>thanhminh92it@gmail.com</td>
                                        </tr>
                                        <tr>
                                            <th>Quyền</th>
                                            <td>Quyền mua bán</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{route('admin-system.user.edit', ['id' => 1])}}" class="btn btn-primary">Sửa</a>
                            <button type="button" class="btn btn-danger delete-user">Xóa</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/AdminSystem/user.js')}}"></script>
@endpush