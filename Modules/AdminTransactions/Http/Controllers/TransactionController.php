<?php
/**
 * Created by PhpStorm.
 * User: Thanh Minh
 * Date: 2/4/2018
 * Time: 14:09
 */

namespace Modules\AdminTransactions\Http\Controllers;


use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    private $db;
    private $object;
    private $pageKey;
    private $records;
    public function __construct()
    {
        $this->db = dbFireBase()->getDatabase();
        $this->object = 'orders';
        $this->records = 10;
        $this->pageKey = pageKey($this->db->getReference($this->object)->getValue(), $this->records);
    }

    public function index()
    {
        $transactions = $this->db->getReference($this->object)->orderByKey()->limitToFirst($this->records)->getSnapshot()->getValue();
        $pages = self::getPages();
        return view('admintransactions::index',compact('transactions', 'pages'));
    }

    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Update file
     */
    public function pagination($currentPage, $records, $search = null)
    {
        $transactions = $this->db->getReference($this->object)->orderByKey()->startAt($this->pageKey[$currentPage-1])->limitToFirst($this->records)->getValue();
        $pages = self::getPages();
        return view('admintransactions::pagination', compact('pages', 'transactions'));
    }

    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Get total page
     */
    public function getPages()
    {
        return ceil($this->db->getReference($this->object)->getSnapshot()->numChildren()/$this->records);
    }
}