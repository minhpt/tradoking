<?php

Route::group(['middleware' => ['web','auth.ptm'], 'prefix' => 'admin', 'namespace' => 'Modules\AdminTransactions\Http\Controllers'], function()
{
    Route::group(['prefix' =>'transactions'], function ()
    {
        //CRUD Transactions
        Route::get('/', 'TransactionController@index')->name('admin-transactions.transaction.index');
        Route::get('/create', 'TransactionController@create')->name('admin-transactions.transaction.create');
        Route::post('/create', 'TransactionController@create')->name('admin-transactions.transaction.create');
        Route::get('/edit/{id}', 'TransactionController@edit')->name('admin-transactions.transaction.edit');
        Route::post('/edit/{id}', 'TransactionController@edit')->name('admin-transactions.transaction.edit');
        Route::get('/detail/{id}', 'TransactionController@detail')->name('admin-transactions.transaction.detail');
        Route::get('/delete/{id}', 'TransactionController@delete')->name('admin-transactions.transaction.delete');

        //Pagination
        Route::get('/pagination/{currentPage}/{records}/{search?}', 'TransactionController@pagination')->name('admin-transactions.transaction.pagination');
    });
});
