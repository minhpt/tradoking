@extends('admin::layouts.master')
@section('title')
    Danh sách giao dịch
@endsection
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/plugins/bootstrap-year-calendar/bootstrap-year-calendar.css')}}">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Giao dịch
            </h1>
            {{--<a href="{{route('admin-room.room.create')}}" class="btn btn-primary btn-box-above">Tạo phòng</a>--}}

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Giao dịch</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content" id="app">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title col-md-2">Danh sách giao dịch</h3>
                            <div class="widget-filter col-md-8 text-center">
                                <span class="filter-name">
                                    Bộ lọc:
                                </span>
                                <span class="filter-name">
                                    <a href="#"><i class="fa fa-circle red"></i> Chờ thanh toán</a>
                                </span>
                                <span class="filter-name">
                                    <a href="#"><i class="fa fa-circle yellow"></i> Chờ xác minh</a>
                                </span>
                                <span class="filter-name">
                                    <a href="#"><i class="fa fa-circle blue"></i> Ký hợp đồng thành công</a>
                                </span>
                            </div>
                            <div class="box-tools">
                                <div class="input-group input-group-sm pull-right" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control pull-right"
                                           placeholder="Search" id="nav-search-input">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-striped table-bordered results-table" data-url="/admin/transactions/pagination">
                                <thead>
                                <tr>
                                    <th class="order-number">STT</th>
                                    <th>Người mua</th>
                                    <th>Người bán</th>
                                    <th>Loại hình</th>
                                    <th>Thông tin phòng</th>
                                    <th>Số hiệu hợp đồng</th>
                                    <th>Trạng thái</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($transactions) > 0)
                                    @php
                                        $order = 1;
                                    @endphp
                                    @foreach($transactions as $key => $transaction)
                                        <tr>
                                            <td>{{$order}}</td>
                                            <td>{{isset($transaction['orderUser']['fullName']) ? $transaction['orderUser']['fullName'] : null}}</td>
                                            <td>{{isset($transaction['orderProduct']['user']['fullName']) ? $transaction['orderProduct']['user']['fullName'] : null}}</td>
                                            <td>{{isset($transaction['orderType']) ? ($transaction['orderType'] == 0 ? 'Mua Bán' : 'Trao Đổi') : null}}</td>
                                            <td>
                                                @isset($transaction['orderProduct'])
                                                    <table class="table table-striped table-bordered">
                                                        <tr>
                                                            <th>Khách sạn</th>
                                                            <td>
                                                                <div>{{$transaction['orderProduct']['hotel']['name']}}</div>
                                                                <div>
                                                                    @for($i = 0; $i < $transaction['orderProduct']['hotel']['starRate']; $i++)
                                                                        <i class="fa fa-star yellow"></i>
                                                                    @endfor
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>Địa chỉ</th>
                                                            <td>
                                                                {{isset($transaction['orderProduct']['hotel']['address']) ? $transaction['orderProduct']['hotel']['address'] : null}}
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <th>Số phòng</th>
                                                            <td>
                                                                {{isset($transaction['orderProduct']['nameOfRoom']) ? $transaction['orderProduct']['nameOfRoom'] : null}}
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <th>Loại phòng</th>
                                                            <td>
                                                                {{isset($transaction['orderProduct']['kindOfRoom']) ? $transaction['orderProduct']['kindOfRoom'] : null}}
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <th>Phòng ngủ</th>
                                                            <td>
                                                                {{isset($transaction['orderProduct']['numberOfBedroom']) ? $transaction['orderProduct']['numberOfBedroom'] : 0}}
                                                            </td>

                                                        </tr>
                                                        <tr>
                                                            <th>Số người</th>
                                                            <td>
                                                                {{isset($transaction['orderProduct']['numberOfPeople']) ? $transaction['orderProduct']['numberOfPeople'] : 0}}
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <th>Giá</th>
                                                            <td>
                                                                {{formatCurrency(isset($transaction['orderProduct']['priceDefault']) ? $transaction['orderProduct']['priceDefault'] : 0)}}
                                                            </td>
                                                        </tr>
                                                    </table>
                                                @endisset
                                            </td>
                                            <td>@isset($transaction['contractNumber']) {{$transaction['contractNumber']}} @endisset</td>
                                            <td>
                                                <select class="form-control" id="status" data-key="{{$transaction['orderId']}}">
                                                    <option value="0" @isset($transaction['statusOrder']) @if($transaction['statusOrder'] == 0) selected @endif @endisset>Chờ thanh toán</option>
                                                    <option value="1" @isset($transaction['statusOrder']) @if($transaction['statusOrder'] == 1) selected @endif @endisset>Chờ xác minh</option>
                                                    <option value="2" @isset($transaction['statusOrder']) @if($transaction['statusOrder'] == 2) selected @endif @endisset>Ký hợp đồng thành công</option>
                                                    <option value="3" @isset($transaction['statusOrder']) @if($transaction['statusOrder'] == 3) selected @endif @endisset>Giao dịch thất bại</option>
                                                </select>
                                            </td>
                                        </tr>
                                        @php
                                            $order++;
                                        @endphp
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7">No Records</td>
                                    </tr>
                                @endif
                                </tbody>

                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="col-md-6 widget-page">
                                @include('admin::pagination.index',['current_page' => 1,'total_page' => $pages])
                            </div>
                            <div class="col-md-6 hide">
                                <select class="form-control pull-right" id="show-records">
                                    <option>10</option>
                                    <option>20</option>
                                    <option>50</option>
                                    <option>100</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            {{--<paginate--}}
                    {{--:page-count="pageTotal"--}}
                    {{--:click-handler="functionName"--}}
                    {{--:prev-text="'Prev'"--}}
                    {{--:next-text="'Next'"--}}
                    {{--:container-class="'pagination'"--}}
                    {{--:page-class="'page-item'">--}}
            {{--</paginate>--}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@stop
@push('scripts')
    <script src="https://unpkg.com/vue-select@latest"></script>
    <script src="{{asset('modules/adminTransactions/transaction.js')}}"></script>
    <script src="{{asset('modules/common/js/pagination-search.js')}}"></script>

    {{--<script src="{{asset('modules/AdminRoom/js/room-no-index.js')}}"></script>--}}
    {{--<script src="{{asset('assets/admin/plugins/bootstrap-year-calendar/bootstrap-year-calendar.js')}}"></script>--}}
@endpush
