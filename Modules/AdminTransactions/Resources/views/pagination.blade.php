@if(count($transactions) > 0)
    @php
        $order = 1;
    @endphp
    @foreach($transactions as $key => $transaction)
        <tr>
            <td>{{$order}}</td>
            <td>{{isset($transaction['orderUser']['fullName']) ? $transaction['orderUser']['fullName'] : null}}</td>
            <td>{{isset($transaction['orderProduct']['user']['fullName']) ? $transaction['orderProduct']['user']['fullName'] : null}}</td>
            <td>{{isset($transaction['orderType']) ? ($transaction['orderType'] == 0 ? 'Mua Bán' : 'Trao Đổi') : null}}</td>
            <td>
                @isset($transaction['orderProduct'])
                    <table class="table table-striped table-bordered">
                        <tr>
                            <th>Khách sạn</th>
                            <td>
                                <div>{{$transaction['orderProduct']['hotel']['name']}}</div>
                                <div>
                                    @for($i = 0; $i < $transaction['orderProduct']['hotel']['starRate']; $i++)
                                        <i class="fa fa-star yellow"></i>
                                    @endfor
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th>Địa chỉ</th>
                            <td>
                                {{isset($transaction['orderProduct']['hotel']['address']) ? $transaction['orderProduct']['hotel']['address'] : null}}
                            </td>

                        </tr>
                        <tr>
                            <th>Số phòng</th>
                            <td>
                                {{isset($transaction['orderProduct']['nameOfRoom']) ? $transaction['orderProduct']['nameOfRoom'] : null}}
                            </td>

                        </tr>
                        <tr>
                            <th>Loại phòng</th>
                            <td>
                                {{isset($transaction['orderProduct']['kindOfRoom']) ? $transaction['orderProduct']['kindOfRoom'] : null}}
                            </td>

                        </tr>
                        <tr>
                            <th>Phòng ngủ</th>
                            <td>
                                {{isset($transaction['orderProduct']['numberOfBedroom']) ? $transaction['orderProduct']['numberOfBedroom'] : 0}}
                            </td>

                        </tr>
                        <tr>
                            <th>Số người</th>
                            <td>
                                {{isset($transaction['orderProduct']['numberOfPeople']) ? $transaction['orderProduct']['numberOfPeople'] : 0}}
                            </td>
                        </tr>
                        <tr>
                            <th>Giá</th>
                            <td>
                                {{formatCurrency(isset($transaction['orderProduct']['priceDefault']) ? $transaction['orderProduct']['priceDefault'] : 0)}}
                            </td>
                        </tr>
                    </table>
                @endisset
            </td>
            <td>@isset($transaction['contractNumber']) {{$transaction['contractNumber']}} @endisset</td>
            <td>
                <select class="form-control" id="status" data-key="{{$transaction['orderId']}}">
                    <option value="0" @isset($transaction['statusOrder']) @if($transaction['statusOrder'] == 0) selected @endif @endisset>Chờ thanh toán</option>
                    <option value="1" @isset($transaction['statusOrder']) @if($transaction['statusOrder'] == 1) selected @endif @endisset>Chờ xác minh</option>
                    <option value="2" @isset($transaction['statusOrder']) @if($transaction['statusOrder'] == 2) selected @endif @endisset>Ký hợp đồng thành công</option>
                    <option value="3" @isset($transaction['statusOrder']) @if($transaction['statusOrder'] == 3) selected @endif @endisset>Giao dịch thất bại</option>
                </select>
            </td>
        </tr>
        @php
            $order++;
        @endphp
    @endforeach
@else
    <tr>
        <td colspan="7">No Records</td>
    </tr>
@endif
<input id="total-pages-current" type="hidden" value="{{ isset($pages) ? $pages : 0 }}">