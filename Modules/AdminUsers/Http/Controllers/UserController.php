<?php

namespace Modules\AdminUsers\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $db;
    private $object;
    private $pageKey;
    private $records;
    private $storage;
    public function __construct()
    {
        $this->db = dbFireBase()->getDatabase();
        $this->storage = dbFireBase()->getStorage();
        $this->object = 'users';
        $this->records = 10;
        $this->pageKey = pageKey($this->db->getReference($this->object)->getValue(), $this->records);
    }

    public function index()
    {
        $users = $this->db->getReference($this->object)->orderByKey()->limitToFirst($this->records)->getSnapshot()->getValue();
        $pages = self::getPages();
        return view('adminusers::index',compact('users','pages'));
    }

    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Get record by page number
     */
    public function pagination($currentPage, $records, $search = null)
    {
        $users = $this->db->getReference($this->object)->orderByKey()->startAt($this->pageKey[$currentPage-1])->limitToFirst($this->records)->getValue();
        $pages = self::getPages();
        return view('adminusers::pagination', compact('pages', 'users'));
    }

    /*
     * @Author: Minhpt
     * @Date: 15/05/2018
     * @Description: Get total page
     */
    public function getPages()
    {
        return ceil($this->db->getReference($this->object)->getSnapshot()->numChildren()/$this->records);
    }

    public function create(Request $request)
    {
        //$users = $this->db->getReference($this->object)->getSnapshot();
        if ($request->isMethod('GET')) {
            return view('adminusers::create');
        }
        $array = $request->all();
        array_shift($array);
        $array['createTime'] = strtotime(date('Y-m-d H:i:s'));
        $array['userType'] = 0;
        $array['password'] = md5($array['password']);
        $key = $this->db->getReference($this->object)->push($array)->getKey();
        $this->db->getReference($this->object. '/'.$key)->update(['userId' => $key]);
        messageSession($request, 'success', 'Thêm thành công');
        return redirect()->route('admin-users.index');
    }

    public function edit(Request $request, $id)
    {
        if ($request->isMethod('GET')) {
            $user = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
            $fileSystem = $this->storage->getFileystem();
            $sizeFile = $fileSystem->getSize($user->getValue()['avatar']);
            $array= explode('/', $user->getValue()['avatar']);
            $nameFile = $array[count($array) - 1];
            if (!$user->exists()) return abort(404);
            return view('adminusers::edit', ['user' => $user, 'sizeFile' => $sizeFile, 'nameFile' => $nameFile]);
        }
        $array = $request->all();
        array_shift($array);
        $array['lastUpdate'] = strtotime(date('Y-m-d H:i:s'));
        $array['password'] = md5($array['password']);
        foreach ($array as $key => $value) {
            $this->db->getReference($this->object .'/' . $id . '/' . $key)->set($value);
        }
        messageSession($request, 'success', 'Cập nhật thành công');
        return redirect()->route('admin-users.user.index');
    }

    public function detail($id)
    {
        $user = $this->db->getReference($this->object . '/' . $id)->getSnapshot();
        return view('adminusers::detail',['user' => $user]);
    }

    public function delete($id)
    {
        $user = $this->db->getReference($this->object . '/' . $id)->remove();
        return response()->json($user);
    }
}