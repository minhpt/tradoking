<?php

Route::group(['middleware' => ['web','auth.ptm'], 'prefix' => 'admin/users', 'namespace' => 'Modules\AdminUsers\Http\Controllers'], function()
{
    Route::get('/', 'UserController@index')->name('admin-users.user.index');
    Route::get('/create', 'UserController@create')->name('admin-users.user.create');
    Route::post('/create', 'UserController@create')->name('admin-users.user.create');
    Route::get('/edit/{id}', 'UserController@edit')->name('admin-users.user.edit');
    Route::post('/edit/{id}', 'UserController@edit')->name('admin-users.user.edit');
    Route::get('/detail/{id}', 'UserController@detail')->name('admin-users.user.detail');
    Route::get('/delete/{id}', 'UserController@delete')->name('admin-users.user.delete');

    //Pagination
    Route::get('/pagination/{currentPage}/{records}/{search?}', 'UserController@pagination')->name('admin-users.transaction.pagination');
});
