@php
    if(isset($user))
        $user = $user->getValue();
@endphp
@push('style')
    <link rel="stylesheet"
          href="{{asset('assets/admin/plugins/bootstrap-toggle-master/css/bootstrap-toggle.min.css')}}">
@endpush
<div class="box-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="col-md-3 control-label">Họ và tên @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="fullName" name="fullName"
                               value="{{isset($user['fullName']) ? $user['fullName'] : null }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label">Giới tính @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <select class="form-control" name="gender">
                            <option {{isset($user['gender']) ? ($user['gender'] == 2 ? 'selected' : null) : null }} value="2">
                                Chưa xác định
                            </option>
                            <option {{isset($user['gender']) ? ($user['gender'] == 1 ? 'selected' : null) : null }} value="1">
                                Nam
                            </option>
                            <option {{isset($user['gender']) ? ($user['gender'] == 0 ? 'selected' : null) : null }} value="0">
                                Nữ
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Mật khẩu @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="password" class="form-control" id="password" name="password"
                               value="{{isset($user['password']) ? $user['password'] : null }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Số điện thoại @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="phoneNumber" name="phoneNumber"
                               value="{{isset($user['phoneNumber']) ? $user['phoneNumber'] : null }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Email @include('require')</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="email" name="email"
                               value="{{isset($user['email']) ? $user['email'] : null }}">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">CMND/Căn cước</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="personalAccountNumber" name="personalAccountNumber"
                               value="{{isset($user['personalAccountNumber']) ? $user['personalAccountNumber'] : null }}">
                    </div>
                </div>
            </div>
            {{--<div class="form-group">--}}
            {{--<label for="inputPassword3" class="col-md-3 control-label">Tài khoản ngân hàng</label>--}}

            {{--<div class="col-md-4">--}}
            {{--<input type="text" class="form-control" id="accountBank" name="accountBank" value="{{isset($user['accountBank']) ? $user['accountBank'] : null }}">--}}
            {{--</div>--}}
            {{--</div>--}}
            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Địa chỉ</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="address" name="address"
                               value="{{isset($user['address']) ? $user['address'] : null }}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Ngày sinh</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="birthday" name="birthday"
                               value="{{isset($user['birthday']) ? $user['birthday'] : null }}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Quốc tịch</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <input type="text" class="form-control" id="nationality" name="nationality"
                               value="{{isset($user['nationality']) ? $user['nationality'] : null }}">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label for="inputPassword3" class="col-md-3 control-label">Trang thái</label>

                <div class="col-md-4">
                    <div class="clearfix">
                        <select class="form-control" name="userStatus">
                            <option {{isset($user['userStatus']) ? ($user['userStatus'] == 0 ? 'selected': null) : null }} value="0">
                                Hoạt Động
                            </option>
                            <option {{isset($user['userStatus']) ? ($user['userStatus'] == 1 ? 'selected': null) : null }} value="1">
                                Khóa
                            </option>
                        </select>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="inputEmail3" class="col-md-2 control-label">Hình ảnh đại diện</label>

                <div class="col-md-5 images">
                    <div id="upload-images-room" class="dropzone"></div>
                </div>
            </div>
        </div>
        <input type="hidden" value="{{isset($user['userId']) ? $user['userId'] : null}}" id="userId">
        <input type="hidden" value="{{isset($sizeFile) ? $sizeFile : null}}" id="sizeFile">
        <input type="hidden" value="{{isset($nameFile) ? $nameFile : null}}" id="nameFile">
        <input type="hidden" value="{{isset($user['avatar']) ? $user['avatar'] : null}}" id="pathFile">
    </div>
</div>
@push('scripts')
    <script src="{{asset('modules/adminUsers/js/user.js')}}"></script>
    <script src="{{asset('modules/adminUsers/js/user-validate.js')}}"></script>
    <script src="{{asset('modules/common/js/firebase.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/dropzone/dropzone.min.js')}}"></script>
    <script src="{{asset('modules/adminUsers/js/image-dropzone.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap-toggle-master/js/bootstrap-toggle.min.js')}}"></script>

@endpush