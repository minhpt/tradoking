@extends('admin::layouts.master')
@section('title')
    Danh sách người dùng
@endsection
@section('style')
    <link rel="stylesheet" href="{{asset('modules/AdminUsers/css/user.css')}}">
@stop
@php
    if(isset($user))
        $user = $user->getValue();
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Người dùng
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Người dùng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-info">
                        <div class="box-header with-border">
                            <h3 class="box-title">Thông tin người dùng</h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6 col-lg-offset-3">
                                    <table class="table table-bordered table-detail">
                                        <tr>
                                            <th class="col-md-6">Avatar</th>
                                            <td class="col-md-6"><img class="thumbnail" style="width: 35%"
                                                                      src="{{asset('images/defaults/No_Image_Available.jpg')}}">
                                            </td>
                                        </tr>
                                        <tr>
                                            <th>Họ và tên</th>
                                            <td>{{$user['fullName']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Số điện thoại</th>
                                            <td>{{$user['phoneNumber']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Email</th>
                                            <td>{{$user['email']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Chứng minh nhân dân</th>
                                            <td>{{$user['personalAccountNumber']}}</td>
                                        </tr>
                                        {{--<tr>--}}
                                            {{--<th>Số tài khoản</th>--}}
                                            {{--<td>9373472386483</td>--}}
                                        {{--</tr>--}}
                                        <tr>
                                            <th>Địa chỉ</th>
                                            <td>{{$user['address']}}</td>
                                        </tr>
                                        <tr>    
                                            <th>Ngày sinh</th>
                                            <td>{{$user['birthday']}}</td>
                                        </tr>
                                        <tr>
                                            <th>Quốc tịch</th>
                                            <td>{{$user['nationality']}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <a href="{{route('admin-users.user.edit', ['id' => $user['userId']])}}" class="btn btn-primary">Sửa</a>
                            <button type="button" class="btn btn-danger delete-user">Xóa</button>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@stop
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/adminUsers/js/user.js')}}"></script>
@endpush