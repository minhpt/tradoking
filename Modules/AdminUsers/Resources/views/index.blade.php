@extends('admin::layouts.master')
@section('title')
    Danh sách người dùng
@endsection
@section('style')
@stop
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Người dùng
            </h1>
            <a href="{{route('admin-users.user.create')}}" class="btn btn-primary btn-box-above">Tạo người dùng</a>

            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-home"></i> Trang chủ</a></li>
                <li class="active">Người dùng</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-danger">
                        <div class="box-header">
                            <h3 class="box-title col-md-2">Danh sách người dùng</h3>
                            <div class="widget-filter col-md-6 text-center">
                                <span class="filter-name">
                                    Bộ lọc:
                                </span>
                                <span class="filter-name">
                                    <a href="#"><span class="label label-success">Kích hoạt</span></a>
                                </span>
                                <span class="filter-name">
                                    <a href="#"><span class="label label-danger">Chưa kích hoạt</span></a>
                                </span>
                            </div>
                            <div class="box-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control pull-right" id="nav-search-input" placeholder="Search">

                                    <div class="input-group-btn">
                                        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table class="table table-hover results-table"  data-url="/admin/users/pagination">
                                <thead>
                                <tr>
                                    <th class="order-number">STT</th>
                                    <th>Họ và tên</th>
                                    <th>Tài khoản</th>
                                    <th>Ngày Sinh</th>
                                    <th>Giới tính</th>
                                    <th>Chứng minh thư</th>
                                    <th>Số điện thoại</th>
                                    <th>Trạng thái</th>
                                    <th class="group-action-3"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(count($users) > 0)
                                    @php
                                        $i = 1;
                                    @endphp
                                    @foreach($users as $key => $user)
                                        <tr>
                                            <td>{{$i}}</td>
                                            <td>{{$user['fullName']}}</td>
                                            <td>{{$user['email']}}</td>
                                            <td>{{$user['birthday']}}</td>
                                            <td>{{$user['gender'] == 1 ? 'Nam' : 'Nữ'}}</td>
                                            <td>{{$user['personalAccountNumber']}}</td>
                                            <td>{{$user['phoneNumber']}}</td>
                                            <td>{!! isset($user['userStatus']) ? userStatues ($user['userStatus']) : null!!}</td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="{{route('admin-users.user.detail', ['id' => $key])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                                    <a href="{{route('admin-users.user.edit', ['id' => $key])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                                                    <button type="button" class="btn btn-danger delete-user"><i class="fa fa-trash-o"></i></button>
                                                </div>
                                            </td>
                                            @php
                                                $i++;
                                            @endphp
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="9">No Records</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer clearfix">
                            <div class="col-md-6 widget-page">
                                @include('admin::pagination.index',['current_page' => 1,'total_page' => $pages])
                            </div>
                            <div class="col-md-6 hide">
                                <select class="form-control pull-right" id="show-records">
                                    <option>10</option>
                                    <option>20</option>
                                    <option>50</option>
                                    <option>100</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

@stop
@push('scripts')
    <!-- DataTables -->
    <script src="{{asset('modules/adminUsers/js/user.js')}}"></script>
    <script src="{{asset('modules/common/js/pagination-search.js')}}"></script>
@endpush
