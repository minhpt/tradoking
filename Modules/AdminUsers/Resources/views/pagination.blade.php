@if(count($users) > 0)
    @php
        $i = 1;
    @endphp
    @foreach($users as $user)
        <tr>
            <td>{{$i}}</td>
            <td>{{$user['fullName']}}</td>
            <td>{{$user['email']}}</td>
            <td>{{$user['birthday']}}</td>
            <td>{{$user['gender'] == 1 ? 'Nam' : 'Nữ'}}</td>
            <td>{{$user['personalAccountNumber']}}</td>
            <td>{{$user['phoneNumber']}}</td>
            <td>{!!$user['userStatus'] == 0 ? '<span class="label label-success">Kích hoạt</span>' : '<span class="label label-danger">Chưa kích hoạt</span>'!!}</td>
            <td>
                <div class="btn-group btn-group-xs">
                    <a href="{{route('admin-users.user.detail', ['id' => $user['userId']])}}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                    <a href="{{route('admin-users.user.edit', ['id' => $user['userId']])}}" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    <button type="button" class="btn btn-danger delete-user"><i class="fa fa-trash-o"></i></button>
                </div>
            </td>
            @php
                $i++;
            @endphp
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="9">No Records</td>
    </tr>
@endif
<input id="total-pages-current" type="hidden" value="{{ isset($pages) ? $pages : 0 }}">