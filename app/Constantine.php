<?php


namespace App;


class Constantine
{
    const Types  = [
        1 => 'Cộng thêm',
        2 => 'Trừ đi',
        3 => 'Giá mới',
        4 => 'Thêm theo % giá gốc',
        5 => 'Trừ theo % giá gốc',
    ];
}