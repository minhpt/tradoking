<?php

use Carbon\Carbon;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

if (!function_exists('dbFireBase')) {
    function dbFireBase() {
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/tradoking-hunghn.json');
//        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/tradoking-local.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();
        return $firebase;
    }
}
if (!function_exists('formatCurrency')) {
    function formatCurrency($cost)
    {
        if ($cost >= 0)
            return number_format($cost, 0) . ' VNĐ';
        return number_format(abs($cost), 0). ' VNĐ';
    }
}
if (!function_exists('messageSession')) {
    function messageSession($request, $status, $msg)
    {
        $request->session()->flash('flash_level', $status);
        $request->session()->flash('flash_message', $msg);
    }
}
if (!function_exists('countDays')) {
    function countDays($dates)
    {
        $count = 0;
        if(is_null($dates)) return $count;
        $array = explode(',', $dates);
        foreach ($array as $item) {
            $tmp = explode('-', $item);
            if (count($tmp) > 1) {
                $dateStart = Carbon::createFromFormat('d/m/Y', $tmp[0]);
                $dateEnd = Carbon::createFromFormat('d/m/Y', $tmp[1]);
                $count += $dateEnd->diffInDays($dateStart);
            }
            else
                $count++;
        }
        return $count;
    }
}
if (!function_exists('formatDate')) {
    function formatDate($date, $new = 'd-m-Y', $old = 'Y-m-d')
    {
        if (is_null($date)) return '-';
        return \Carbon\Carbon::createFromFormat($old, $date)->format($new);
    }
}
if (!function_exists('arrayPush')) {
    function arrayPush($items)
    {
        $array = [];
        if ($items->exists()) {
            $items = $items->getValue();
            foreach ($items as $item) {
                array_push($array, $item);
            }
        }
        return $array;
    }
}
if (!function_exists('user')) {
    function user()
    {
        return session('user');
    }
}
if (!function_exists('pageKey')) {
    function pageKey($array, $records)
    {
        $results = [];
        $key = array_keys($array);
        for ($i = 0; $i < count($key); $i = $i + $records)
        {
            array_push($results, $key[$i]);
        }
        return $results;
    }
}
if (!function_exists('userStatues')) {
    function userStatues($params)
    {
        switch ($params)
        {
            case 1:
                return '<span class="label label-success">Registered</span>';
                break;
            case 2:
                return '<span class="label label-danger">Vip</span>';
                break;
            default:
                return '<span class="label label-info">News</span>';
                break;
        }
    }
}