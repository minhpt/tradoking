$(function () {
    activeMenu('categories', 'extension', true);
    $('.view-delete').click(function () {
        var element = $(this);
        var row = element.closest('tr');
        $.confirm({
            title: 'Xác nhận thông tin!',
            content: 'Bạn có thực sự muốn xóa tiện ích ' + element.data('extension') + '?',
            buttons: {
                Yes: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                        $.ajax({
                            url: '/admin/extensions/delete/' + element.data('id'),
                            type: 'get',
                            success: function (result) {
                                console.log(result);
                                $('.alert-success').show();
                                $('.alert-danger').hide();
                                row.remove();
                            }
                        })
                    }
                },
                No: {
                    text: 'Hủy',
                    btnClass: 'btn-default',
                    action: function(){
                    }
                }
            }
        });
    });
});