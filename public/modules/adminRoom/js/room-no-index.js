$(function () {
    activeMenu('rooms',null, false);
    addDatePicker('.reservationtime');
    addDatePicker('.available-date');
    $('.view-multiple').select2();
    $('.delete-room').click(function () {
        console.log(1);
        $.confirm({
            title: 'Xác nhận thông tin!',
            content: 'Bạn có thực sự muốn xóa phòng 3001 - khách sạn Mường Thanh?',
            buttons: {
                Yes: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                    }
                },
                No: {
                    text: 'Hủy',
                    btnClass: 'btn-default',
                    action: function(){
                    }
                }
            }
        });
    });
    $('.avatar').click(function () {
        $('#avatar').click();
    });
    $('#add-cost').click(function (e) {
        e.preventDefault();
       var elements = $('#add-row-table-cost tbody').html();
       $('#table-room-costs tbody').append(elements);
        addDatePicker('.reservationtime');
    });
    $(document).on('click','.remove-cost', function () {
        var element = $(this).closest('tr');
        $.confirm({
            title: 'Xác nhận thông tin!',
            content: 'Bạn có thực sự muốn giá này không?',
            buttons: {
                Yes: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                        element.remove();
                    }
                },
                No: {
                    text: 'Hủy',
                    btnClass: 'btn-default',
                    action: function(){
                    }
                }
            }
        });

    });
    $('#add-time').click(function (e) {
        e.preventDefault();
        var elements = $('#add-row-table-time tbody').html();
        $('#table-room-times tbody').append(elements);
        addDatePicker('.available-date');
    });
    $(document).on('click','.remove-time', function () {
        var element = $(this).closest('tr');
        $.confirm({
            title: 'Xác nhận thông tin!',
            content: 'Bạn có thực sự muốn khoảng thời gian này không?',
            buttons: {
                Yes: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                        element.remove();
                        var day = parseInt($('.times').text());
                        var arrayDay = element.find('.available-date').val().split(' - ');
                        var startDay = toDate(arrayDay[0]);
                        var endDay = toDate(arrayDay[1]);
                        var result = day - resultDays(startDay, endDay) - 1;
                        $('.times').text(result);
                        $('#times').val(result);
                    }
                },
                No: {
                    text: 'Hủy',
                    btnClass: 'btn-default',
                    action: function(){
                    }
                }
            }
        });
    });
    getDataAdress();
    $('#hotels').change(function () {
        getDataAdress();
    });
});
function getDataAdress() {
    var element = $('#hotels').find(':selected');
    $('.address').text(element.data('address'));
    $('.latitude').val(element.data('latitude'));
    $('.longitude').val(element.data('longitude'));
}
function addDatePicker(element) {
    $(element).daterangepicker({
        timePicker: false,
        timePickerIncrement: 30,
        autoUpdateInput: false,
        autoProcessQueue: false,
        locale: {
            "format": "DD/MM/YYYY",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
        },
        "opens": "center" });
    $(element).on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
        var day = 0;
        $('#table-room-times .available-date').each(function (key, value) {
            var arrayDay = $(this).val().split(' - ');
            var startDay = toDate(arrayDay[0]);
            var endDay = toDate(arrayDay[1]);
            day = day + resultDays(startDay, endDay) + 1;
        });
        $('.times').text(day);
        $('#times').val(day);
    });

    $(element).on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
    });
}
Dropzone.autoDiscover = false;
var dropzoneImages = new Dropzone("#upload-images-room", {
    url: '/admin/upload',
    dictDefaultMessage: '<i class="fa fa-file fa-4x" aria-hidden="true"></i><br><h2>Chọn tập tin</h2><span>hoặc kéo thả tập tin</span>',
    paramName: 'imageFilesRoom',
    addRemoveLinks: true,
    dictRemoveFile: "Xóa tập tin",
    maxFiles: 10,
    maxFilesize: 10,
    previewTemplate: '<li class="working list-group-item">' +
    '<span data-dz-name></span> <span data-dz-size></span> (<a target="_blank" class="download-file-image" href="#"><i class="fa fa-download"></i> Tải xuống</a>)' +
    '<div class="progress" style="margin-top: 10px; margin-bottom: 0;">' +
    '<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" data-dz-uploadprogress></div>' +
    '</div></li>',
    acceptedFiles: "image/*",
    init: function () {
        var dropZone = this;
        this.on("maxfilesexceeded", function (file) {
            this.removeFile(file);
        });
        var data = getFiles($('#productId').val());
        if (data !== null && !isEmpty(data.images)) {
            $.each(data.images, function (key, value) {
                var mockFile = {
                    name: value['name'],
                    size: parseInt(value['size']),
                    accepted: true
                };
                dropZone.emit('addedfile', mockFile);
                dropZone.emit("success", mockFile);
                dropZone.emit("complete", mockFile);
                $('.download-file-image').eq(key).attr('href','https://firebasestorage.googleapis.com/v0/b/tradoking-92b53.appspot.com/o/' + encodeURIComponent(value['path']) + '?alt=media');
                dropZone.files.push(mockFile);
            });
        }
        this.on("success", function (file, response) {
            var d = new Date();
            var n = d.getTime();
            var path = 'products/' + $('#productId').val() +'/images';
            var pathFull = path + '/' + n + '_' + file.name;
            $('.images').append('<input type="hidden" value="'+ pathFull +'" name="images[]">' +
                '<input type="hidden" value="'+ file.name +'" name="imageNames[]">' +
                '<input type="hidden" value="'+ file.size +'" name="imageSizes[]">'
            );
            putFileToFirebase(file, path, n);

            var element = file.previewTemplate;
            element.getElementsByClassName('download-file-image')[0].href = 'https://firebasestorage.googleapis.com/v0/b/tradoking-92b53.appspot.com/o/' + encodeURIComponent(pathFull) + '?alt=media';
        });
    },
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var dropzoneDocuments = new Dropzone("#upload-documents-room", {
    url: '/admin/upload',
    dictDefaultMessage: '<i class="fa fa-file fa-4x" aria-hidden="true"></i><br><h2>Chọn tập tin</h2><span>hoặc kéo thả tập tin</span>',
    paramName: 'documentFilesRoom',
    addRemoveLinks: true,
    dictRemoveFile: "Xóa Tập Tin",
    previewTemplate: '<li class="working list-group-item">' +
    '<span data-dz-name></span> <span data-dz-size></span> (<a target="_blank" class="download-file-document" href="#"><i class="fa fa-download"></i> Tải xuống</a>)' +
    '<div class="progress" style="margin-top: 10px; margin-bottom: 0;">' +
    '<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" data-dz-uploadprogress></div>' +
    '</div></li>',
    maxFiles: 10,
    maxFilesize: 10,
    acceptedFiles: "image/*, .pdf",
    init: function () {
        var dropZone = this;
        dropZone.on("maxfilesexceeded", function (file) {
            this.removeFile(file);
        });
        var data = getFiles($('#productId').val());
        if (data !== null && !isEmpty(data.documents)) {
            $.each(data.documents, function (key, value) {
                var mockFile = {
                    name: value['name'],
                    size: parseInt(value['size']),
                    accepted: true
                };
                dropZone.emit('addedfile', mockFile);
                dropZone.emit("success", mockFile);
                dropZone.emit("complete", mockFile);
                $('.download-file-document').eq(key).attr('href','https://firebasestorage.googleapis.com/v0/b/tradoking-92b53.appspot.com/o/' + encodeURIComponent(value['path']) + '?alt=media');
                dropZone.files.push(mockFile);
            });
        }
        this.on("success", function (file, response) {
            var d = new Date();
            var n = d.getTime();
            var path = 'products/' + $('#productId').val() + '/documents';
            var pathFull = path + '/' + n + '_' + file.name;
            $('.documents').append('<input type="hidden" value="'+ pathFull +'" name="documents[]">' +
                '<input type="hidden" value="'+ file.name +'" name="documentNames[]">' +
                '<input type="hidden" value="'+ file.size +'" name="documentSizes[]">'
            );
            putFileToFirebase(file, path, n)

            var element = file.previewTemplate;
            element.getElementsByClassName('download-file-document')[0].href = 'https://firebasestorage.googleapis.com/v0/b/tradoking-92b53.appspot.com/o/' + encodeURIComponent(pathFull) + '?alt=media';
        });
    },
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var config = {
    apiKey: "AIzaSyBi7pb9Xm1dtRbd2ztQ1okHxZN0RFGBzRo",
    authDomain: "tradoking-92b53.firebaseapp.com",
    databaseURL: "https://tradoking-92b53.firebaseio.com",
    projectId: "tradoking-92b53",
    storageBucket: "tradoking-92b53.appspot.com",
    messagingSenderId: "803878142073"
};
firebase.initializeApp(config);
function putFileToFirebase(file, path, time) {
    var storageRef = firebase.storage().ref(path + '/' + time + '_' + file.name);
    storageRef.put(file);
}

function getFiles(id) {
    var result = null;
    $.ajax({
        type: 'GET',
        async: false,
        url: '/admin/room/get-files/' + id,
        success: function (data) {
            result = data
        }
    });
    return result;
}
