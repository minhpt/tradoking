$(function () {
    // var table = $('#rooms').DataTable({
    //     'paging'      : true,
    //     'lengthChange': true,
    //     'ordering'    : false,
    //     'info'        : true,
    //     'autoWidth'   : true
    // });
    var currentYear = new Date().getFullYear();
    $('.calendar').each(function (key, value) {
        $('#calendar' + (key + 1)).calendar({
            mouseOnDay: function(e) {
                if(e.events.length > 0) {
                    var content = e.events[0].status == true ? 'Khách hàng đã đặt' : 'Ngày còn lại';

                    $(e.element).popover({
                        trigger: 'manual',
                        container: 'body',
                        html:true,
                        content: content
                    });

                    $(e.element).popover('show');
                }
            },
            mouseOutDay: function(e) {
                if(e.events.length > 0) {
                    $(e.element).popover('hide');
                }
            },
            dataSource: [
                {
                    id: 0,
                    name: 'Google I/O',
                    location: 'San Francisco, CA',
                    startDate: new Date(currentYear, 4, 18),
                    endDate: new Date(currentYear, 4, 21),
                    color: '#3c8dbc',
                    status: true
                },
                {
                    id: 1,
                    name: 'Microsoft Convergence',
                    location: 'New Orleans, LA',
                    startDate: new Date(currentYear, 4, 26),
                    endDate: new Date(currentYear, 4, 29),
                    color: '#3c8dbc',
                    status: true
                },
                {
                    id: 2,
                    name: 'Microsoft Convergence',
                    location: 'New Orleans, LA',
                    startDate: new Date(currentYear, 4, 18),
                    endDate: new Date(currentYear, 4, 29),
                    color: '#ff9900',
                    status: false
                },
                {
                    id: 3,
                    name: 'Google I/O',
                    location: 'San Francisco, CA',
                    startDate: new Date(currentYear, 8, 18),
                    endDate: new Date(currentYear, 8, 21),
                    color: '#3c8dbc',
                    status: true
                },
                {
                    id: 4,
                    name: 'Microsoft Convergence',
                    location: 'New Orleans, LA',
                    startDate: new Date(currentYear, 8, 26),
                    endDate: new Date(currentYear, 8, 29),
                    color: '#3c8dbc',
                    status: true
                },
                {
                    id: 5,
                    name: 'Microsoft Convergence',
                    location: 'New Orleans, LA',
                    startDate: new Date(currentYear, 8, 18),
                    endDate: new Date(currentYear, 8, 29),
                    color: '#ff9900',
                    status: false
                }

            ]
        });
    });

    $('.detail-night').click(function (e) {
        e.preventDefault();
        var index = $(this).closest('tr').index() / 2;
        console.log(index);
       if($(this).hasClass('fa-angle-double-up'))
       {
           $(this).removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
           $('#rooms tbody tr.detail-calendar:eq('+index+')').show();
           $('.month-container').removeClass('col-xs-12').addClass('col-xs-2');
       }
       else{
           $(this).removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
           $('#rooms tbody tr.detail-calendar:eq('+index+')').hide();
       }
    });

    $('.room-delete').click(function () {
        var element = $(this);
        var row = element.closest('tr');
        $.confirm({
            title: 'Xác nhận thông tin!',
            content: 'Bạn có thực sự muốn xóa phòng ' + element.data('room') + '?',
            buttons: {
                Yes: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                        $.ajax({
                            url: '/admin/room/delete/' + element.data('room-id'),
                            type: 'get',
                            success: function (result) {
                                console.log(result);
                                $('.alert-success').show();
                                $('.alert-danger').hide();
                                row.remove();
                            }
                        })
                    }
                },
                No: {
                    text: 'Hủy',
                    btnClass: 'btn-default',
                    action: function(){
                    }
                }
            }
        });
    });
});