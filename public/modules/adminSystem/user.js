$(function () {
   activeMenu('system','user-admin', true);
    $('.user-delete').click(function () {
        var element = $(this);
        var row = element.closest('tr');
        $.confirm({
            title: 'Xác nhận thông tin!',
            content: 'Bạn có thực sự muốn xóa tài khoản ' + element.data('user') + '?',
            buttons: {
                Yes: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                        $.ajax({
                            url: '/admin/users/delete/' + element.data('id'),
                            type: 'get',
                            success: function (result) {
                                console.log(result);
                                $('.alert-success').show();
                                $('.alert-danger').hide();
                                row.remove();
                            }
                        })
                    }
                },
                No: {
                    text: 'Hủy',
                    btnClass: 'btn-default',
                    action: function(){
                    }
                }
            }
        });
    });
});