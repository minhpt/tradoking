Vue.component('paginate', VuejsPaginate);
Vue.component('v-select', VueSelect.VueSelect);
/* global Vue, firebase */
var db = _db.database();
var recordsPage = 2;
var recordsTotal = Object.keys(db.ref('orders')).length;
var pageCurrent = 1;
var vue = new Vue({
    el: '#app',
    firebase: {
        orders: {
            source: db.ref('orders'),
        }
        // name:  db.ref('orders').child('-L7TyIZ51eIgeREvOBrE').child('orderProduct'),
    },
    data: {
        recordsPage: 2,
        recordsTotal: Object.keys(db.ref('orders')).length,
        pageCurrent: 1,
        selectedStatus : [],
        options: [
            {id: 0, label: 'Chờ thanh toán'},
            {id: 1, label: 'Chờ xác minh'},
            {id: 2, label: 'Ký hợp đồng thành công'},
            {id: 3, label: 'Giao dịch thất bại'},
        ],
        selected: []
    }
    ,
    computed: {
        pageTotal: function () {
          return Math.ceil(this.recordsTotal/this.recordsPage)
      }
    },
    methods: {
        formatCurrency : function (price) {
            return (parseFloat(price)).toLocaleString('vi', {
                style: 'currency',
                currency: 'VND'
            });
        },
        orderType: function (type) {
            return type === 0 ? 'Mua bán' : 'Trao đổi';
        },
        functionName: function (pageNum) {
            this.pageCurrent = pageNum
        },
        statusChange: function (order, key) {
            db.ref('orders').child(order['.key']).child('statusOrder').set(this.selected[key].id);

        }
    }
});
$(function () {
   $('#status').change(function () {
       db.ref('orders').child($(this).data('key')).child('statusOrder').set(parseInt($(this).val()));
   });
});
