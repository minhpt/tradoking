var config = {
    apiKey: "AIzaSyBi7pb9Xm1dtRbd2ztQ1okHxZN0RFGBzRo",
    authDomain: "tradoking-92b53.firebaseapp.com",
    databaseURL: "https://tradoking-92b53.firebaseio.com",
    projectId: "tradoking-92b53",
    storageBucket: "tradoking-92b53.appspot.com",
    messagingSenderId: "803878142073"
};
firebase.initializeApp(config);
function putFileToFirebase(file, path, time) {
    var storageRef = firebase.storage().ref(path + '/' + time + '_' + file.name);
    storageRef.put(file);
}
Dropzone.autoDiscover = false;
var dropzoneImages = new Dropzone("#upload-images-room", {
    url: '/admin/upload',
    dictDefaultMessage: '<i class="fa fa-file fa-4x" aria-hidden="true"></i><br><h2>Chọn tập tin</h2><span>hoặc kéo thả tập tin</span>',
    paramName: 'imageFilesRoom',
    addRemoveLinks: true,
    dictRemoveFile: "Xóa tập tin",
    maxFiles: 10,
    maxFilesize: 1,
    previewTemplate: '<li class="working list-group-item">' +
    '<span data-dz-name></span> <span data-dz-size></span> (<a target="_blank" class="download-file-image" href="#"><i class="fa fa-download"></i> Tải xuống</a>)' +
    '<div class="progress" style="margin-top: 10px; margin-bottom: 0;">' +
    '<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" data-dz-uploadprogress></div>' +
    '</div></li>',
    acceptedFiles: "image/*",
    init: function () {
        var dropZone = this;
        this.on("maxfilesexceeded", function (file) {
            this.removeFile(file);
        });
        var nameFile = $('#nameFile').val();
        var sizeFile = $('#sizeFile').val();
        var pathFile = $('#pathFile').val();
        if (nameFile !== null && nameFile !== '') {
            var mockFile = {
                name: nameFile,
                size: sizeFile,
                accepted: true
            };
            dropZone.emit('addedfile', mockFile);
            dropZone.emit("success", mockFile);
            dropZone.emit("complete", mockFile);
            $('.download-file-image').eq(0).attr('href','https://firebasestorage.googleapis.com/v0/b/tradoking-92b53.appspot.com/o/' + encodeURIComponent(pathFile) + '?alt=media');
            dropZone.files.push(mockFile);
        }
        this.on("success", function (file, response) {
            var date = new Date();
            var time = date.getTime();
            var path = 'users';
            var pathFull = path + '/' + time + '_' + file.name;
            $('.images').append('<input type="hidden" value="'+ pathFull +'" name="avatar">'
            );
            putFileToFirebase(file, path, time);

            var element = file.previewTemplate;
            element.getElementsByClassName('download-file-image')[0].href = 'https://firebasestorage.googleapis.com/v0/b/tradoking-92b53.appspot.com/o/' + encodeURIComponent(pathFull) + '?alt=media';
        });
    },
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
