$(function () {
    activeMenu('users',null, false);
    $('.delete-user').click(function () {
        $.confirm({
            title: 'Xác nhận thông tin!',
            content: 'Bạn có thực sự muốn xóa tài khoản thanhminh92it@gmail.com?',
            buttons: {
                Yes: {
                    text: 'Xóa',
                    btnClass: 'btn-danger',
                    keys: ['enter', 'shift'],
                    action: function(){
                    }
                },
                No: {
                    text: 'Hủy',
                    btnClass: 'btn-default',
                    action: function(){
                    }
                }
            }
        });
    });

    $('.avatar').click(function () {
       $('#avatar').click();
    });
});