$(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('.alert').delay(5000).slideUp(200, function () {
        $(this).hide();
    });
});

function activeMenu(parent, children, treeview) {
    $('.sidebar-menu > li').removeClass('active');
    $('.' + parent).addClass('active');
    if (treeview) {
        $('.' + children).addClass('active');
    }
}

function resultDays(startDay,endDay){
    var millisecondsPerDay = 1000 * 60 * 60 * 24;
    var millisBetween = endDay.getTime() - startDay.getTime();
    var days = millisBetween / millisecondsPerDay;
    // Round down.
    return Math.floor(days);

}
function toDate(dateStr) {
    var parts = dateStr.split("/");
    return new Date(parts[2], parts[1] - 1, parts[0]);
}

function isEmpty(obj) {
    for (var key in obj) {
        if (obj.hasOwnProperty(key))
            return false;
    }
    return true;
}

var _db = firebase.initializeApp({
    databaseURL: 'https://tradoking-5f117.firebaseio.com'
});